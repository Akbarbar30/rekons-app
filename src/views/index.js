import {
  Breadcrumbs,
  Cards,
  Carousels,
  Collapses,
  Dropdowns,
  Forms,
  Jumbotrons,
  ListGroups,
  Navbars,
  Navs,
  Paginations,
  Popovers,
  ProgressBar,
  Switches,
  Tables,
  Tabs,
  Tooltips
} from "./Base"

import { ButtonDropdowns, ButtonGroups, Buttons, BrandButtons } from "./Buttons"
import Charts from "./Charts"
import Dashboard from "./Dashboard"
import { CoreUIIcons, Flags, FontAwesome, SimpleLineIcons } from "./Icons"
import { Alerts, Badges, Modals } from "./Notifications"
import Login from "./Login/login"

import { Login, Page404, Page500, Register, Project, MyReports, Users, SendEmail, General, Features, EmailOptions, LDAP, LoginPage, NewUser, AllTasks, Tickets, Discussion, TaskReport, GanttChart, LabelConfig,confTools, TypeConfig, StatusConfig} from "./Pages"

import { Colors, Typography } from "./Theme"
import Widgets from "./Widgets"

export {
  Login,
  Badges,
  Typography,
  Colors,
  CoreUIIcons,
  Page404,
  Page500,
  Modals,
  Alerts,
  Flags,
  SimpleLineIcons,
  FontAwesome,
  ButtonDropdowns,
  ButtonGroups,
  BrandButtons,
  Buttons,
  Tooltips,
  Tabs,
  Tables,
  Charts,
  Dashboard,
  Widgets,
  Jumbotrons,
  Switches,
  ProgressBar,
  Popovers,
  Navs,
  Navbars,
  ListGroups,
  Forms,
  Dropdowns,
  Collapses,
  Carousels,
  Cards,
  Breadcrumbs,
  Paginations
}
