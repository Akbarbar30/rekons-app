import React from "react"
import TableByRek from "./table_byrekening";
import TableByBank from "./table_bybank";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import SearchDate from "../../../components/searchdate/searchdate"

function TabContainer({ children, dir }) {
    return (
        <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
            {children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
};

const styles = theme => ({
    textField: {
        marginLeft: 0,
        marginRight: 0,
        width: '150px',
        fontFamily: 'Avenir-Black',
        color: '#1890ff',
    },
    dropdown: {
        float: 'right'
    },
    input: {
        height: '10px'
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 100,
        fontFamily: 'Avenir-Black',
        color: '#1890ff',
    },
    root: {
        boxShadow: '0px',
        width: '100%',
    },
    root1: {
        boxShadow: '0px',
        width: '100%',
    },
    tabsIndicator: {
        background: '20px #FFD34B',
        height: '5px',
    },
    tabRoot: {
        textTransform: 'initial',
        fontSize: '20px',
        minWidth: 100,
        color: '#FFFFFF',
        backgroundColor: '#34B966',
        fontFamily: [
            'Avenir-Black',
        ].join(','),
        '&:hover': {
            color: '#FFFFFF',
            opacity: 1,
        },
        '&$tabSelected': {
            fontFamily: 'Avenir-Black',
            color: '#FFFFFF',
            fontWeight: '600'
        },
        '&:focus': {
            fontFamily: 'Avenir-Black',
            color: '#FFFFFF',
        },
    },
    tabSelected: {},
    typography: {
        fontFamily: 'Avenir-Black',
        padding: theme.spacing.unit * 3,
    },
});
class ExportMainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            databyRek: [],
            databyBank: [],
            isLoading: true,
            start_date: null,
            end_date: null
        }

        this.fetchApi = this.fetchApi.bind(this);


    }

    fetchApi = async (start,end) => {
        this.setState({isLoading: true})
        await fetch(`http://rekons.immobisp.com:3005/rekons/rekap/exportdata/${start}/${end}`)
            .then(listResp => {
                return listResp.json()
            })
            .then(listFile => {
                this.setState({
                    databyRek: listFile.by_akunRek,
                    databyBank: listFile.by_akunBank,
                    start_date: start,
                    end_date: end,
                    isLoading:false
                })
            })
            .catch(err => console.log(err))

            console.log("state : ", this.state)
    }


    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { classes } = this.props;
        return (
            <>
                <SearchDate onSubmit={this.fetchApi}/>
                {!this.state.isLoading ? (
                    <>
                        <div className={classes.root}>
                            <AppBar className={classes.root1} position="static" color="none" elevation={1}>

                                <Tabs
                                    value={this.state.value}
                                    onChange={this.handleChange}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    variant="fullWidth"
                                    classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                                    style={{ fontFamily: 'Avenir-Black' }}
                                >
                                    <Tab
                                        classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                        style={{ fontFamily: 'Avenir-Black' }}
                                        label="Berdasarkan Akun Rekening Masjid"
                                    />
                                    <Tab
                                        classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                        style={{ fontFamily: 'Avenir-Black' }}
                                        label="Berdasarkan Nama Bank"
                                    />
                                </Tabs>

                            </AppBar>
                            {this.state.value === 0 && <TabContainer><TableByRek start_date={this.state.start_date} end_date={this.state.end_date} data={this.state.databyRek} /></TabContainer>}
                            {this.state.value === 1 && <TabContainer><TableByBank start_date={this.state.start_date} end_date={this.state.end_date} data={this.state.databyBank} /></TabContainer>}

                        </div>
                    </>
                ) : (
                        <div></div>
                    )}

            </>

        )
    }
}

export default withStyles(styles, { withTheme: true })(ExportMainPage);