import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import ReactExport from "react-export-excel";
import moment from "moment"

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class OvoMainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listFile: [],
            isLoading: false
        }
    }

    async componentDidMount() {
        this.setState({ isLoading: true })
        await fetch('http://rekons.immobisp.com:3005/rekons/rekap/alldata')
            .then(listResp => {
                return listResp.json()
            })
            .then(listFile => {

                this.setState({ listFile: listFile.rekap_MP, isLoading: false })
            })
            .catch(err => console.log(err))
    }

    render() {
        const dataSet1 = this.state.listFile;
        
        return (
            <>
                {!this.state.isLoading ? (
                    <>
                        <div>
                            <ExcelFile 
                            element={
                                <Button color="primary" className="btn-add-new" size="sm">
                                    Export Excel
                                </Button>}
                            filename={`${moment().format('L')}-transaksiBank`}    
                            >
                                <ExcelSheet data={dataSet1} name="Employees">
                                    <ExcelColumn label="Nama Bank" value="bank_penerima" />
                                    <ExcelColumn label="Nama Rekening Penerima" value="nama_rekening_penerima" />
                                    <ExcelColumn label="No Rekening" value="no_rekening_penerima" />
                                    <ExcelColumn label="Total Nominal" value="total_akhir" />
                                </ExcelSheet>
                            </ExcelFile>
                        </div>
                    </>
                ) : (
                        <h1>Tidak ada data</h1>
                    )}
            </>
        );
    }
}

OvoMainPage.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default OvoMainPage
