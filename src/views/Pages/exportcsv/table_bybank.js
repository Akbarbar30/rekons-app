import React from "react";
import MUIDataTable from "mui-datatables"
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles"
import { Button } from "reactstrap";
import DialogListData from "../../../components/dialog/export_data"


class TableByBank extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isDialogOpen: false,
            listdata: [],
            rekapmasjid: [],
            bank: "",
            nominal_transaksi_awal : 0,
            nominal_potongan_kmdn : 0.0,
            nominal_potongan_channel : 0.0,
            nominal_potongan_cashback: 0.0,
            total_akhir: 0
        };
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
            MUITableHead: {
                root: {
                    boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)",
                },
            },
            MUIDataTableHeadCell: {
                root: {
                    whiteSpace: 'nowrap',
                },
            },
            MUIDataTableBodyCell: {
                root: {
                    whiteSpace: 'nowrap'
                }
            }
        }
    });

    async handleDialogOpen(bank, nominal_transaksi_awal, nominal_potongan_kmdn, nominal_potongan_channel, nominal_potongan_cashback, total_akhir) {
        const { start_date, end_date } = this.props
        await fetch(`http://rekons.immobisp.com:3005/rekons/rekap/getDataExportByBank/${bank}/${start_date}/${end_date}`)
            .then(detailRekonsResp => {
                return detailRekonsResp.json()
            })
            .then(detailRekons => {
                this.setState({
                    listdata: detailRekons.data_export,
                    rekapmasjid: detailRekons.data_masjid,
                    bank,
                    nominal_transaksi_awal,
                    nominal_potongan_kmdn,
                    nominal_potongan_channel,
                    nominal_potongan_cashback,
                    total_akhir,
                    isDialogOpen: true
                })
            })
            .catch(error => console.log(error))
        console.log(this.state)
    }


    handleDialogClose = value => {
        this.setState({ isDialogOpen: false })
    }

    columns = [
        {
            name: "nama_bank",
            id: "nama_bank",
            label: "Nama Bank",
            options: {
                sort: false
            }
        },
        {
            name: "jumlah_transaksi",
            id: "jumlah_transaksi",
            label: "Jumlah Transaksi",
            options: {
                sort: false
            }
        },
        {
            name: "nominal_transaksi_awal",
            id: "nominal_transaksi_awal",
            label: "Total Nominal Transaksi Awal",
            options: {
                sort: false
            }
        },
        {
            name: "nominal_potongan_kmdn",
            id: "nominal_potongan_kmdn",
            label: "Total Nominal Potongan KMDN",
            options: {
                sort: false
            }
        },
        {
            name: "nominal_potongan_channel",
            id: "nominal_potongan_channel",
            label: "Total Nominal Potongan Channel",
            options: {
                sort: false
            }
        },
        {
            name: "rejeki_poin",
            id: "rejeki_poin",
            label: "Total Nominal Rejeki Poin",
            options: {
                sort: false
            }
        },
        {
            name: "total_akhir",
            id: "total_akhir",
            label: "Total Nominal Akhir",
            options: {
                sort: false
            }
        },
        {
            name: "export",
            id: "export",
            label: "List Data",
            options: {
                sort: false,
                customBodyRender: (value) => {
                    return (
                        <div>
                            <span>
                                <Button color="primary" style={{ whiteSpace: "nowrap" }} className="btn-add-new" size="sm" onClick={() => this.handleDialogOpen(value[0], value[1], value[2], value[3], value[4], value[5])}>
                                    List Data
                                </Button>
                            </span>
                        </div>
                    );
                }
            }
        },
    ];

    data = this.props.data.map(d => [
        `${d.nama_bank === "null" ? " " : d.nama_bank}`,
        parseInt(`${d.jumlah_transaksi}`),
        parseInt(`${d.nominal_transaksi_awal}`),
        `${d.nominal_potongan_kmdn}`,
        `${d.nominal_potongan_channel}`,
        `${d.nominal_potongan_cashback}`,
        parseInt(`${d.total_akhir}`),
        [`${d.nama_bank}`, parseInt(`${d.nominal_transaksi_awal}`), `${d.nominal_potongan_kmdn}`,
            `${d.nominal_potongan_channel}`,
            `${d.nominal_potongan_cashback}`,
            parseInt(`${d.total_akhir}`)]

    ])

    options = {
        filter: true,
        selectableRows: false,
        print: false,
        filterType: false,
        pagination: true,
        elevation: 0,
        rowsPerPage: 10,
        responsive: 'scroll',
        download: false,
        search: true,
        viewColumns: false
    };

    render() {
        return (
            <div>
                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable title="Rekap Bank" data={this.data} columns={this.columns} options={this.options} />
                </MuiThemeProvider>
                <DialogListData
                    data={this.state.listdata}
                    rekapmasjid={this.state.rekapmasjid}
                    handleDialogClose={this.handleDialogClose}
                    isDialogOpen={this.state.isDialogOpen}
                    namaRek={this.state.bank}
                    nominal_transaksi_awal={this.state.nominal_transaksi_awal}
                    nominal_potongan_kmdn={this.state.nominal_potongan_kmdn}
                    nominal_potongan_channel={this.state.nominal_potongan_channel}
                    nominal_potongan_cashback={this.state.nominal_potongan_cashback}
                    total_akhir={this.state.total_akhir}
                />
            </div>
        )
    }
}

export default TableByBank