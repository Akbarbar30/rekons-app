import React from "react"
import MUIDataTable from "mui-datatables"
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles"
import { FormGroup, FormLabel, TextField } from '@material-ui/core';
import moment from "moment"
import { Button } from "reactstrap"
import ExportDataByFilter from "../../../components/dialog/export_data_byfilter"

class OVOUploadCSV extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            rp_awal: 0,
            rp_akhir:0,
            listData: [],
            isDialogOpen: false
        }
        this.handleTableChange = this.handleTableChange.bind(this);
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
            MUITableHead: {
                root: {
                    boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)",
                },
            },
            MUIDataTableHeadCell: {
                root: {
                    whiteSpace: 'nowrap',
                },
            },
            MUIDataTableBodyCell: {
                root: {
                    whiteSpace: 'nowrap'
                }
            },

        }
    });

    handleTableChange = async (action, tableState) => {
        var array = []
        console.log(tableState)
        await array.push(tableState.displayData.map(ts => ts.data.map(d => d)))
       
        await this.setState({ listData: array[0] })
        await console.log("listData", this.state.listData)
        var nominal_awal = []
        var nominal_akhir = []

        array[0].map(d => {
            nominal_awal.push(d[4]);
            nominal_akhir.push(d[6]);
        })
        var total_nominal_awal = 0
        var total_nominal_akhir = 0
        this.setState({ tr: nominal_awal.length })
        nominal_awal.map((item) => {
            total_nominal_awal = item + total_nominal_awal;
        })
        nominal_akhir.map((item) => {
            total_nominal_akhir = item + total_nominal_akhir;
        })
        this.setState({ rp_awal: total_nominal_awal, rp_akhir: total_nominal_akhir })
    };

    handleDialogOpen = () => {
        this.setState({ isDialogOpen: true });
    };

    handleDialogClose = () => {
        this.setState({ isDialogOpen: false });
    };

    columns = [
        {
            name: "NO",
            id: "no",
            label: "ID",
            options: {
                filter: false,
                sort: true
            }
        },
        {
            name: "channel",
            id: "channel",
            label: "Channel",
            options: {
                filter: true,
                sort: true
            }
        },
        {
            name: "reference_id",
            id: "reference_id",
            label: "Reference ID",
            options: {
                filter: false,
                sort: false
            }
        },
        {
            name: "tgl_bayar",
            id: "tgl_bayar",
            label: "Tanggal Pembayaran",
            options: {
                filter: true,
                filterType: 'custom',
                customFilterListRender: v => {
                    if (v['min'] && v['max']) {
                        return `Start Date: ${v['min']}, End Date: ${v['max']}`;
                    } else if (v['min']) {
                        return `Start Date: ${v['min']}`;
                    } else if (v['max']) {
                        return `End Date: ${v['max']}`;
                    }
                    return false;
                },
                filterOptions: {
                    names: [],
                    logic(tanggal, filters) {
                        var tgl = new Date(tanggal)
                        var max = new Date(filters['max'])
                        var min = new Date(filters['min'])
                        if (filters['min'] && filters['max']) {
                            return tgl < min || tgl > max;
                        } else if (filters['min']) {
                            return tgl < min;
                        } else if (filters['max']) {
                            return tgl > max;
                        }
                        return false;
                    },
                    display: (filterList, onChange, index, column) => (
                        <div>
                            <FormLabel>Tanggal Pembayaran</FormLabel>
                            <FormGroup row>
                                <TextField
                                    label="Start Date"
                                    type="date"
                                    value={filterList[index]['min'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['min'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%', marginRight: '5%' }}
                                />
                                <TextField
                                    label="End Date"
                                    type="date"
                                    value={filterList[index]['max'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['max'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%' }}
                                />
                            </FormGroup>
                        </div>
                    ),
                },
            },
        },
        {
            name: "total_nominal_awal",
            id: "total_nominal_awal",
            label: "Nominal Awal",
            options: {
                filter: false,
                sort: true
            },
        },
        {
            name: "nominal_potongan_channel",
            id: "nominal_potongan_channel",
            label: "Nominal Potongan Channel",
            options: {
                filter: false,
                sort: true
            },
        },
        {
            name: "total_nominal_akhir",
            id: "total_nominal_akhir",
            label: "Nominal Akhir",
            options: {
                filter: false,
                sort: true
            },
        },
        {
            name: "customer_name",
            id: "customer_name",
            label: "Customer Contact",
            options: {
                filter: false,
                sort: true
            },
        },
        {
            name: "status",
            id: "status",
            label: "Status",
            options: {
                filter: false,
                sort: true
            },
        },
        {
            name: "rekons",
            id: "rekons",
            label: "Terekons",
            options: {
                filter: true,
                sort: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    if (value === 1) {
                        return (
                            <i className={`fas fa-check`} style={{ color: 'green' }} />
                        )
                    } else if (value === 0) {
                        return <div></div>
                    }
                },
                filterOptions: {
                    names: ['Tidak Terekons', 'Sudah Terekons'],
                    logic(rekon, filterVal) {
                        const show =
                            (filterVal.indexOf('Tidak Terekons') >= 0 && rekon === 0) ||
                            (filterVal.indexOf('Sudah Terekons') >= 0 && rekon === 1);
                        return !show;
                    },
                },
            }
        }
    ];

    data = this.props.rekapImport.map((file, index) => [
        parseInt(`${index + 1}`),
        `${file.channel}`,
        `${file.reference_id}`,
        `${moment(file.payment_date).format("YYYY-MM-DD HH:mm:ss")}`,
        parseInt(`${file.payment_amount}`),
        parseFloat(`${file.nominal_potongan_channel}`),
        parseFloat(`${file.payment_amount - file.nominal_potongan_channel}`),
        `${file.customer}`,
        `${file.status}`,
        parseInt(`${file.isSame}`)
    ])

    options = {
        filter: true,
        selectableRows: 'none',
        print: false,
        filterType: 'multiselect',
        pagination: true,
        elevation: 0,
        rowsPerPage: 10,
        responsive: 'scroll',
        onTableInit: this.handleTableChange,
        onTableChange: this.handleTableChange,
        download: false
    };

    render() {

        return (
            <>
                {this.props.rekapImport ? (
                    <>
                        <div>
                            <Button color="primary" className="btn-add-new" size="sm" onClick={this.handleDialogOpen}>
                                <i className="fa fa-plus" style={{ marginRight: 5, fontSize: 10 }} /> Export  Data
                            </Button>
                            <MuiThemeProvider theme={this.getMuiTheme()}>
                                <MUIDataTable title="Data Import" data={this.data} columns={this.columns} options={this.options} />
                            </MuiThemeProvider>
                            <h4>Total Transaksi = {this.state.tr}</h4>
                            <h4>Total Nominal Awal = Rp. {this.state.rp_awal}</h4>
                            <h4>Total Potongan Channel = Rp. {(this.state.rp_awal- this.state.rp_akhir.toFixed(1)).toFixed(1)}</h4>
                            <h4>Total Nominal Akhir = Rp. {this.state.rp_akhir.toFixed(1)}</h4>
                            <ExportDataByFilter
                                handleDialogClose={this.handleDialogClose}
                                isDialogOpen={this.state.isDialogOpen}
                                data={this.state.listData}
                            />
                        </div>
                    </>
                ) : (
                        <div>

                        </div>
                    )}

            </>

        );
    }
}

export default OVOUploadCSV 