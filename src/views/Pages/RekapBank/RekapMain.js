import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import RekapBankTab from './RekapBank_Table';
import RekapRekTab from './RekapRekening_Table'
import TrImportTab from './RekapImport_Table';
import TrMuslimpocketTab from './RekapMP_Table';
import CircularProgress from '@material-ui/core/CircularProgress';

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

const styles = theme => ({
  textField: {
    marginLeft: 0,
    marginRight: 0,
    width: '150px',
    fontFamily: 'Avenir-Black',
    color: '#1890ff',
  },
  dropdown: {
    float: 'right'
  },
  input: {
    height: '10px'
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 100,
    fontFamily: 'Avenir-Black',
    color: '#1890ff',
  },
  root: {
    boxShadow: '0px',
    width: '100%',
  },
  root1: {
    boxShadow: '0px',
    width: '100%',
  },
  tabsIndicator: {
    background: '20px #FFD34B',
    height: '5px',
  },
  tabRoot: {
    textTransform: 'initial',
    whiteSpace: 'nowrap',
    fontSize: '20px',
    minWidth: 100,
    color: '#FFFFFF',
    backgroundColor: '#34B966',
    fontFamily: [
      'Avenir-Black',
    ].join(','),
    '&:hover': {
      color: '#FFFFFF',
      opacity: 1,
    },
    '&$tabSelected': {
      fontFamily: 'Avenir-Black',
      color: '#FFFFFF',
      fontWeight: '600'
    },
    '&:focus': {
      fontFamily: 'Avenir-Black',
      color: '#FFFFFF',
    },
  },
  tabSelected: {},
  typography: {
    fontFamily: 'Avenir-Black',
    padding: theme.spacing.unit * 3,
  },
  loading:{
    color: '#34B966',
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%,-50%)',
    width: '5%!important',
    height: '5%!important'
  }
});

class RekapMainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      rekapBank: [],
      rekapImport: [],
      rekapMP: [],
      rekapRek:[],
      isLoading: false
    }
  }

  async componentDidMount() {
    this.setState({ isLoading: true })
    await fetch('http://rekons.immobisp.com:3005/rekons/rekap/alldata')
      .then(Resp => {
        return Resp.json()
      })
      .then(resp => {
        this.setState({ rekapRek: resp.rekap_rekening, rekapMP: resp.rekap_MP, rekapImport: resp.rekap_Import, rekapBank: resp.rekap_bank, isLoading: false })
      })
      .catch(err => console.log(err))
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;

    return (
      <>
        {!this.state.isLoading ? (
          <>
            <div className={classes.root}>
              <AppBar className={classes.root1} position="static" color="none" elevation={1}>

                <Tabs
                  value={this.state.value}
                  onChange={this.handleChange}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="fullWidth"
                  classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                  style={{ fontFamily: 'Avenir-Black' }}
                >
                   <Tab
                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                    style={{ fontFamily: 'Avenir-Black' }}
                    label="Rekap Rekening"
                  />
                  <Tab
                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                    style={{ fontFamily: 'Avenir-Black' }}
                    label="Rekap Bank"
                  />
                  <Tab
                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                    style={{ fontFamily: 'Avenir-Black' }}
                    label="Rekap Transaksi Import"
                  />
                  <Tab
                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                    style={{ fontFamily: 'Avenir-Black' }}
                    label="Rekap Transaksi Muslimpocket"
                  />
                </Tabs>

              </AppBar>
              {this.state.value === 0 && <TabContainer><RekapRekTab data={this.state.rekapRek} /></TabContainer>}
              {this.state.value === 1 && <TabContainer><RekapBankTab rekapBank={this.state.rekapBank} /></TabContainer>}
              {this.state.value === 2 && <TabContainer><TrImportTab rekapImport={this.state.rekapImport} /></TabContainer>}
              {this.state.value === 3 && <TabContainer><TrMuslimpocketTab rekapMP={this.state.rekapMP} /></TabContainer>}

            </div>
          </>
        ) : (
            <div>
               <CircularProgress  className={classes.loading}/>
            </div>
          )}
      </>

    );
  }
}


export default withStyles(styles, { withTheme: true })(RekapMainPage);
