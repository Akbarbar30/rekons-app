import React from "react";
import MUIDataTable from "mui-datatables"
import { createMuiTheme, MuiThemeProvider, withStyles } from "@material-ui/core/styles"
import { FormGroup, FormLabel, TextField } from '@material-ui/core';
import DialogListData from "../../../components/dialog/export_data"
import DialogDataExport from "../../../components/dialog/export_data_byrek"
import EditDataRek from "../../../components/dialog/edit_norek";
import { Button } from "reactstrap";

class RekapRekTable extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            isDialogOpen: false,
            nama: "",
            norek: '',
            listData: [],
            DataExport: [],
            rekapmasjid: [],
            nominal_transaksi_awal: 0,
            nominal_potongan_kmdn: 0.0,
            nominal_potongan_channel: 0.0,
            nominal_potongan_cashback: 0.0,
            total_akhir: 0,
            isDialogOpenExport: false,
            isDialogOpenEdit: false,
            DetailMasjid: []
        }

        this.handleTableChange = this.handleTableChange.bind(this);
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
            MUITableHead: {
                root: {
                    boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)",
                },
            },
            MUIDataTableHeadCell: {
                root: {
                    whiteSpace: 'nowrap',
                },
            },
            MUIDataTableBodyCell: {
                root: {
                    whiteSpace: 'nowrap'
                }
            },
            MUIDataTableToolbar: {
                filterPaper: {
                    width: '30%'
                }
            }
        }
    });


    handleTableChange = async (action, tableState) => {
        var array = []
        console.log(tableState)
        await array.push(tableState.displayData.map(ts => ts.data.map(d => d)))

        await this.setState({ DataExport: array[0] })
        console.log(this.state.DataExport)
    };

    async handleDialogOpen(receiver, nominal_transaksi_awal, nominal_potongan_kmdn, nominal_potongan_channel, nominal_potongan_cashback, total_akhir) {
        console.log("edit untuk norek : ", receiver)
        console.log("norek tidak ada")
        await fetch(`http://rekons.immobisp.com:3005/rekons/rekap/getDataExport/${receiver}`)
            .then(detailRekonsResp => {
                return detailRekonsResp.json()
            })
            .then(detailRekons => {
                this.setState({
                    listData: detailRekons.data_export,
                    nama: receiver,
                    rekapmasjid: detailRekons.data_masjid,
                    nominal_transaksi_awal,
                    nominal_potongan_kmdn,
                    nominal_potongan_channel,
                    nominal_potongan_cashback,
                    total_akhir,
                    isDialogOpen: true
                })
            })
            .catch(error => console.log(error))
    }

    async handleDialogOpenEdit(receiver) {
        console.log("edit untuk norek : ", receiver)
        await fetch(`http://rekons.immobisp.com:3005/rekons/rekap/detail/${receiver}`)
            .then(detailMasjidResp => {
                return detailMasjidResp.json()
            })
            .then(detailMasjid => {
                this.setState({
                    DetailMasjid: detailMasjid.detail_masjid[0],
                    isDialogOpenEdit: true
                })
                console.log(this.state.DetailMasjid)
            })
            .catch(error => console.log(error))
    }

    handleDialogClose = value => {
        this.setState({ isDialogOpen: false })
    }

    handleDialogCloseEdit = value => {
        this.setState({ isDialogOpenEdit: false })
    }

    handleDialogCloseExport = value => {
        this.setState({ isDialogOpenExport: false })
    }

    handleDialogOpenExport = value => {
        this.setState({ isDialogOpenExport: true })
    }

    columns = [
        {
            name: "id",
            id: "id",
            label: "No",
            options: {
                sort: true,

            }
        },
        {
            name: "username_penerima",
            id: "username_penerima",
            label: "Username Penerima",
            options: {
                sort: false,
                filter: false
            }
        },
        {
            name: "nama_penerima",
            id: "nama_penerima",
            label: "Nama Penerima",
            options: {
                sort: false,
                filter: false
            }
        },
        {
            name: "bank_penerima",
            id: "bank_penerima",
            label: "Bank Penerima",
            options: {
                sort: false,
                filter: false
            }
        },
        {
            name: "no_rekening_penerima",
            id: "no_rekening_penerima",
            label: "No Rekening Penerima",
            options: {
                sort: false,
                filter: true,
                filterOptions: {
                    names: ['Tidak Lengkap', 'Lengkap'],
                    logic(rek_penerima, filterVal) {
                        const show =
                            (filterVal.indexOf('Tidak Lengkap') >= 0 && (rek_penerima === "" || rek_penerima === " ")) ||
                            (filterVal.indexOf('Lengkap') >= 0 && (rek_penerima !== "" && rek_penerima !== " "));
                        return !show;
                    },
                },
            }
        },
        {
            name: "nama_rekening_penerima",
            id: "nama_rekening_penerima",
            label: "Nama Rekening Penerima",
            options: {
                sort: false,
                filter: false
            }
        },
        {
            name: "jumlah_transaksi",
            id: "jumlah_transaksi",
            label: "Jumlah Transaksi",
            options: {
                sort: false,
                filter: false
            }
        },
        {
            name: "nominal_transaksi_awal",
            id: "nominal_transaksi_awal",
            label: "Nominal Transaksi Awal",
            options: {
                sort: true,
                filter: true,
                filterType: 'custom',
                customFilterListRender: v => {
                    if (v['min']) {
                        return `Minimal Nominal: Rp ${v['min']}`;
                    }
                    return false;
                },
                filterOptions: {
                    names: [],
                    logic(nominal, filters) {
                        var check = parseInt(nominal);
                        var min = parseInt(filters['min']);

                        if (filters['min'] && check <= min) {
                            return true;
                        }
                        return false;
                    },
                    display: (filterList, onChange, index, column) => (
                        <div>
                            <FormLabel>Minimal Nominal</FormLabel>
                            <FormGroup row>
                                <TextField
                                    label=""
                                    type="text"
                                    value={filterList[index]['min'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['min'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '100%' }}
                                />
                            </FormGroup>
                        </div>
                    ),
                }
            }
        },
        {
            name: "nominal_potongan",
            id: "nominal_potongan",
            label: "Nominal Potongan",
            options: {
                sort: false,
                filter: false,
            }
        },
        {
            name: "nominal_transaksi_akhir",
            id: "nominal_transaksi_akhir",
            label: "Nominal Transaksi Akhir",
            options: {
                sort: true,
                filter: false,

            }
        },
        {
            name: "action",
            label: "Action",
            options: {
                filter: false,
                sort: false,
                customBodyRender: (value) => {
                    return (
                        <div>
                            <span>
                                <Button color="primary" style={{ whiteSpace: "nowrap" }} className="btn-add-new" size="sm" onClick={() => this.handleDialogOpenEdit(value[0])}>
                                    Edit Detail
                                </Button>
                            </span>
                            <span>
                                <Button color="primary" style={{ whiteSpace: "nowrap" }} className="btn-add-new" size="sm" onClick={() => this.handleDialogOpen(value[0], value[1], value[2], value[3], value[4], value[5])}>
                                    Export Data
                                </Button>
                            </span>
                        </div>
                    );
                }
            }
        }
    ];

    data = this.props.data.map((file, index) => [
        parseInt(`${index + 1}`),
        `${file.receiver === "null" ? " " : file.receiver}`,
        `${file.nama_penerima === "null" ? " " : file.nama_penerima}`,
        `${file.bank_penerima === "null" ? " " : file.bank_penerima}`,
        `${file.no_rekening_penerima === "null" ? " " : file.no_rekening_penerima}`,
        `${file.nama_rekening_penerima === "null" ? " " : file.nama_rekening_penerima}`,
        `${file.jumlah_transaksi}`,
        parseInt(`${file.nominal_transaksi_awal}`),
        parseFloat(`${file.nominal_transaksi_awal - file.total_akhir}`),
        parseFloat(`${file.total_akhir}`),
        [`${file.receiver}`, parseInt(`${file.nominal_transaksi_awal}`), `${file.nominal_potongan_kmdn}`,
        `${file.nominal_potongan_channel}`,
        `${file.nominal_potongan_cashback}`,
        parseInt(`${file.total_akhir}`)]
    ])

    options = {
        filter: true,
        selectableRows: 'none',
        print: false,
        filterType: 'multiselect',
        pagination: true,
        elevation: 0,
        rowsPerPage: 10,
        responsive: 'scroll',
        onTableInit: this.handleTableChange,
        onTableChange: this.handleTableChange,
        download: false
    };

    render() {
        return (
            <div>
                <Button color="primary" className="btn-add-new" size="sm" onClick={this.handleDialogOpenExport}>
                    <i className="fa fa-plus" style={{ marginRight: 5, fontSize: 10 }} /> Export  Data
                </Button>
                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable title="Rekap Rekening Masjid" data={this.data} columns={this.columns} options={this.options} />
                </MuiThemeProvider>
                <DialogListData
                    data={this.state.listData}
                    handleDialogClose={this.handleDialogClose}
                    isDialogOpen={this.state.isDialogOpen}
                    namaRek={this.state.nama}
                    rekapmasjid={this.state.rekapmasjid}
                    nominal_transaksi_awal={this.state.nominal_transaksi_awal}
                    nominal_potongan_kmdn={this.state.nominal_potongan_kmdn}
                    nominal_potongan_channel={this.state.nominal_potongan_channel}
                    nominal_potongan_cashback={this.state.nominal_potongan_cashback}
                    total_akhir={this.state.total_akhir}
                />
                <DialogDataExport
                    data={this.state.DataExport}
                    handleDialogClose={this.handleDialogCloseExport}
                    isDialogOpen={this.state.isDialogOpenExport}
                />
                <EditDataRek
                    detailDataRek={this.state.DetailMasjid}
                    handleDialogClose={this.handleDialogCloseEdit}
                    isDialogOpen={this.state.isDialogOpenEdit}
                />
            </div>
        );

    }
}

export default RekapRekTable