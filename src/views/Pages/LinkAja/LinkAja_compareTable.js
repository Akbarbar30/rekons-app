import React from "react"
import MUIDataTable from "mui-datatables"
import { createMuiTheme, MuiThemeProvider , withStyles } from "@material-ui/core/styles"
import { Container, Row, Col } from 'reactstrap';
import { FormGroup, FormLabel, TextField } from '@material-ui/core';
import moment from "moment";
import classnames from 'classnames';

const customStyles = {
  isMatchRow: {
    '& td': {backgroundColor: "indianred"}
  }
};

class LinkAjaExportCSV extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            total_nominal_faspay: 0,
            total_transaksi_faspay: 0,
            total_nominal_mpOvo: 0,
            total_transaksi_mpOvo: 0
        }

        this.handleTableChangeFaspay = this.handleTableChangeFaspay.bind(this);
        this.handleTableChangeMP = this.handleTableChangeMP.bind(this);
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
            MUITableHead: {
                root: {
                    boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)",
                },
            },
            MUIDataTableHeadCell: {
                root: {
                    whiteSpace: 'nowrap',
                },
            },
            MUIDataTableBodyCell: {
                root: {
                    whiteSpace: 'nowrap'
                }
            },

        }
    });

    handleTableChangeFaspay = (action, tableState) => {
        var array = []
        array.push(tableState.displayData.map(ts => ts.data.map(d => d)))
        var duit = []
        array[0].map(d => duit.push(d[3]))
        var total_nominal = 0
        this.setState({ total_transaksi_faspay: duit.length })
        duit.map((item) => {
            total_nominal = item + total_nominal;

        })
        this.setState({ total_nominal_faspay: total_nominal })
    };

    handleTableChangeMP = (action, tableState) => {
        var array = []
        array.push(tableState.displayData.map(ts => ts.data.map(d => d)))
        var duit = []
        array[0].map(d => duit.push(d[3]))
        var total_nominal = 0
        this.setState({ total_transaksi_mpOvo: duit.length })
        duit.map((item) => {
            total_nominal = item + total_nominal;
        })
        this.setState({ total_nominal_mpOvo: total_nominal })
    };

    columnsFaspay = [
        {
            name: "NO",
            id: "no",
            label: "ID",
            options: {
                filter: false,
                sort: true
            }
        },
        {
            name: "bill_no",
            id: "bill_no",
            label: "Transaction ID",
            options: {
                filter: false,
                sort: true
            }
        },
        {
            name: "Tanggal Pembayaran",
            id: "tgl_bayar",
            label: "Tanggal Pembayaran",
            options: {
                filter: true,
                filterType: 'custom',
                customFilterListRender: v => {
                    if (v['min'] && v['max']) {
                        return `Start Date: ${v['min']}, End Date: ${v['max']}`;
                    } else if (v['min']) {
                        return `Start Date: ${v['min']}`;
                    } else if (v['max']) {
                        return `End Date: ${v['max']}`;
                    }
                    return false;
                },
                filterOptions: {
                    names: [],
                    logic(tanggal, filters) {
                        var check = new Date(tanggal);
                        var from = new Date(filters['min']);
                        var to = new Date(filters['max']);
                        from.setDate(from.getDate());
                        to.setDate(to.getDate());
                        from = new Date(from).setHours(0, 0, 0, 0);
                        to = new Date(to).setHours(23, 59, 59, 59);

                        if (filters['min'] && filters['max'] && check >= to && check <= from) {
                            return true;
                        } else if (filters['min'] && check >= to) {
                            return true;
                        } else if (filters['max'] && check <= from) {
                            return true;
                        }
                        return false;
                    },
                    display: (filterList, onChange, index, column) => (
                        <div>
                            <FormLabel>Tanggal Pembayaran</FormLabel>
                            <FormGroup row>
                                <TextField
                                    label="Start Date"
                                    type="date"
                                    value={filterList[index]['min'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['min'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%', marginRight: '5%' }}
                                />
                                <TextField
                                    label="End Date"
                                    type="date"
                                    value={filterList[index]['max'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['max'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%' }}
                                />
                            </FormGroup>
                        </div>
                    ),
                },
            },
        },
        {
            name: "total_bayar",
            id: "total_bayar",
            label: "Total Pembayaran",
            options: {
                filter: false,
                sort: true
            },
        },
        {
            name: "status",
            id: "status",
            label: "Status",
            options: {
                filter: false,
                sort: true
            },
        },
        {
            name: "rekons",
            id: "rekons",
            label: "Terekons",
            options: {
                filter: false,
                sort: false,
                display: "excluded"
            },
        }
    ];

    dataFaspay = this.props.dataImport.map((file, index) => [
        parseInt(`${index + 1}`),
        `${file.bill_no === 0 ? "" : parseInt(file.bill_no)}`,
        `${moment(file.payment_date).format("YYYY-MM-DD HH:mm:ss")}`,
        parseInt(`${file.payment_amount}`),
        `${file.status}`,
        parseInt(`${file.isSame}`)
    ])

    optionsFaspay = {
        filter: true,
        selectableRows: 'none',
        print: false,
        filterType: 'multiselect',
        pagination: true,
        elevation: 0,
        rowsPerPage: 10,
        responsive: 'scroll',
        onTableInit: this.handleTableChangeFaspay,
        onTableChange: this.handleTableChangeFaspay,
        download: false,
        setRowProps: (row) => {
            console.log(row[5])
            return {
                className: classnames(
                    {
                        [this.props.classes.isMatchRow]: row[5] === 0
                    })
            };
        }
    };

    columnsMP = [
        {
            name: "NO",
            id: "no",
            label: "ID",
            options: {
                filter: false,
                sort: true
            }
        },
        {
            name: "bill_no",
            id: "bill_no",
            label: "Transaction ID",
            options: {
                filter: false,
                sort: true
            }
        },
        {
            name: "tgl_bayar",
            id: "tgl_bayar",
            label: "Tanggal Bayar",
            options: {
                filter: true,
                sort: true,
                filterType: 'custom',
                customFilterListRender: v => {
                    if (v['min'] && v['max']) {
                        return `Start Date: ${v['min']}, End Date: ${v['max']}`;
                    } else if (v['min']) {
                        return `Start Date: ${v['min']}`;
                    } else if (v['max']) {
                        return `End Date: ${v['max']}`;
                    }
                    return false;
                },
                filterOptions: {
                    names: [],
                    logic(tanggal, filters) {
                        var check = new Date(tanggal);
                        var from = new Date(filters['min']);
                        var to = new Date(filters['max']);
                        from.setDate(from.getDate());
                        to.setDate(to.getDate());
                        from = new Date(from).setHours(0, 0, 0, 0);
                        to = new Date(to).setHours(23, 59, 59, 59);

                        if (filters['min'] && filters['max'] && check >= to && check <= from) {
                            return true;
                        } else if (filters['min'] && check >= to) {
                            console.log(new Date(check).toString());
                            console.log(new Date(to).toString());
                            return true;
                        } else if (filters['max'] && check <= from) {
                            console.log(new Date(check).toString());
                            console.log(new Date(from).toString());
                            return true;
                        }
                        return false;
                    },
                    display: (filterList, onChange, index, column) => (
                        <div>
                            <FormLabel>Tanggal Pembayaran</FormLabel>
                            <FormGroup row>
                                <TextField
                                    label="Start Date"
                                    type="date"
                                    value={filterList[index]['min'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['min'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%', marginRight: '5%' }}
                                />
                                <TextField
                                    label="End Date"
                                    type="date"
                                    value={filterList[index]['max'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['max'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%' }}
                                />
                            </FormGroup>
                        </div>
                    ),
                },
            },
        },
        {
            name: "total_bayar",
            id: "total_bayar",
            label: "Total Pembayaran",
            options: {
                filter: false,
                sort: true
            },
        },
        {
            name: "status",
            id: "status",
            label: "Status",
            options: {
                filter: false,
                sort: true
            },
        }
    ];

    dataMP = this.props.dataMP.map((file, index) => [
        parseInt(`${index + 1}`),
        parseInt(`${file.bill_no}`),
        `${moment(file.tgl_pembayaran).format("YYYY-MM-DD HH:mm:ss")}`,
        parseInt(`${file.total_pembayaran}`),
        `${file.status}`
    ])

    optionsMP = {
        filter: true,
        selectableRows: 'none',
        print: false,
        filterType: 'multiselect',
        pagination: true,
        elevation: 0,
        rowsPerPage: 10,
        responsive: 'scroll',
        onTableInit: this.handleTableChangeMP,
        onTableChange: this.handleTableChangeMP,
        download: false
    };

    render() {
        return (
            <div>
                <Container>
                    <Row>
                        <Col md='6'>
                            <MuiThemeProvider theme={this.getMuiTheme()}>
                                <MUIDataTable title="Data Import LinkAja" data={this.dataFaspay} columns={this.columnsFaspay} options={this.optionsFaspay} />
                            </MuiThemeProvider>
                            <h4>Total Transaksi = {this.state.total_transaksi_faspay}</h4>
                            <h4>Total Nominal = Rp. {this.state.total_nominal_faspay}</h4>
                        </Col>

                        <Col md='6'>
                            <MuiThemeProvider theme={this.getMuiTheme()}>
                                <MUIDataTable title="Data LinkAja Muslimpocket" data={this.dataMP} columns={this.columnsMP} options={this.optionsMP} />
                                <h4>Total Transaksi = {this.state.total_transaksi_mpOvo}</h4>
                                <h4>Total Nominal = Rp. {this.state.total_nominal_mpOvo}</h4>
                            </MuiThemeProvider>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default withStyles(customStyles, {name: "LinkAjaExportCSV.js"})(LinkAjaExportCSV)