import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import LinkAjaTab from './LinkAja_dataLinkAja';
import LinkAjaMPTab from './LinkAja_dataMP';
import ComparisonTableTab from './LinkAja_compareTable';
import RealTimeTab from './LinkAja_dataRealtime';
import CircularProgress from '@material-ui/core/CircularProgress';

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

const styles = theme => ({
  textField: {
    marginLeft: 0,
    marginRight: 0,
    width: '150px',
    fontFamily: 'Avenir-Black',
    color: '#1890ff',
  },
  dropdown: {
    float: 'right'
  },
  input: {
    height: '10px'
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 100,
    fontFamily: 'Avenir-Black',
    color: '#1890ff',
  },
  root: {
    boxShadow: '0px',
    width: '100%',
  },
  root1: {
    boxShadow: '0px',
    width: '100%',
  },
  tabsIndicator: {
    background: '20px #FFD34B',
    height: '5px',
  },
  tabRoot: {
    textTransform: 'initial',
    fontSize: '20px',
    minWidth: 100,
    color: '#FFFFFF',
    backgroundColor: '#34B966',
    fontFamily: [
      'Avenir-Black',
    ].join(','),
    '&:hover': {
      color: '#FFFFFF',
      opacity: 1,
    },
    '&$tabSelected': {
      fontFamily: 'Avenir-Black',
      color: '#FFFFFF',
      fontWeight: '600'
    },
    '&:focus': {
      fontFamily: 'Avenir-Black',
      color: '#FFFFFF',
    },
  },
  tabSelected: {},
  typography: {
    fontFamily: 'Avenir-Black',
    padding: theme.spacing.unit * 3,
  },
  loading:{
    color: '#34B966',
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%,-50%)',
    width: '5%!important',
    height: '5%!important'
  }
});

class LinkAjaMainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      dataMP: [],
      dataImport: [],
      dataRealTime: [],
      isLoading: false
    }
  }

  async componentDidMount() {
    this.setState({ isLoading: true })
    await Promise.all(
      [
        fetch(`http://rekons.immobisp.com:3005/rekons/rekap/databychannel/linkaja`, { method: 'GET' }),
        fetch(`http://rekons.immobisp.com:3005/rekons/linkaja/datarealtime`)
      ]
    ).then(([dataRekonsResp, dataRealtimeResp]) => {
      return Promise.all([dataRekonsResp.json(), dataRealtimeResp.json()])
    }).then(([dataRekons, dataRealtime]) => {
      this.setState({
        dataMP: dataRekons.data_MP,
        dataImport: dataRekons.data_Import,
        dataRealTime: dataRealtime.result,
        isLoading: false
      })
    }).catch(error => console.log(error));

  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;

    return (
      <>
        {!this.state.isLoading ? (
          <>
            <div className={classes.root}>
              <AppBar className={classes.root1} position="static" color="none" elevation={1}>

                <Tabs
                  value={this.state.value}
                  onChange={this.handleChange}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="fullWidth"
                  classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                  style={{ fontFamily: 'Avenir-Black' }}
                >
                  <Tab
                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                    style={{ fontFamily: 'Avenir-Black' }}
                    label="Transaksi Realtime"
                  />
                  <Tab
                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                    style={{ fontFamily: 'Avenir-Black' }}
                    label="Transaksi LinkAja"
                  />
                  <Tab
                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                    style={{ fontFamily: 'Avenir-Black' }}
                    label="Transaksi Muslimpocket"
                  />
                  <Tab
                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                    style={{ fontFamily: 'Avenir-Black' }}
                    label="Tabel Perbandingan"
                  />
                </Tabs>

              </AppBar>
              {this.state.value === 0 && <TabContainer><RealTimeTab dataRealtime={this.state.dataRealTime} /></TabContainer>}
              {this.state.value === 1 && <TabContainer><LinkAjaTab dataImport={this.state.dataImport} /></TabContainer>}
              {this.state.value === 2 && <TabContainer><LinkAjaMPTab dataMP={this.state.dataMP} /></TabContainer>}
              {this.state.value === 3 && <TabContainer><ComparisonTableTab dataImport={this.state.dataImport} dataMP={this.state.dataMP} /></TabContainer>}

            </div>
          </>
        ) : (
            <div>
              <CircularProgress  className={classes.loading}/>
            </div>
          )}
      </>
    );
  }
}

LinkAjaMainPage.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(LinkAjaMainPage);