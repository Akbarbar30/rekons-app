import React, { Component } from "react"
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';

import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Label,
  Row
} from "reactstrap"

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      amount: '',
      response:null,
      email: '',
      password: '',
      weight: '',
      weightRange: '',
      showPassword: false,
      remember: null,
    };
  }

  async componentDidMount(){
    if (await localStorage.getItem('token')) return this.props.history.push("/dashboard")
  }

  handleChange = (e) => {
    const target = e.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
    this.setState({
        [name]: value
    })
  }

  handleLogin = async event => {
    const url = `http://rekons.immobisp.com:3005/rekons/login/`

    const form = await {
      userID : this.state.email,
      password : this.state.password
    }

    await fetch(url, { method: "POST",
    body: JSON.stringify(form),
    headers: { 
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    } })
    .then(res => {
      return res.json()})
    .then(response => {
      this.setState({response})})
    .catch(error => console.error('Error:', error));
    
    console.log(this.state.response)
    if(this.state.response.token){
       Promise.all([localStorage.setItem('user', JSON.stringify(this.state.response)), localStorage.setItem('token', this.state.response.token) ])
                .then(() => {
                  this.props.history.push("/dashboard")
                })
    }else{
      window.alert("Login Error")
    }
   
  }

  render() {
    return (
      <div className="app flex-row align-items-center login-background">
        {/* <img src ={loginBackground} width= '100%' height='100%'/> */}
        <Container>
          <Row className="justify-content-center">
            <Col md="5" style={{position:'absolute', right:'25rem', top:'9rem'}}>
              <Card className="p-5">
                <CardBody>
                  <h4 style={{color:"#237d45", fontWeight:'bold', position:'relative', bottom:'0.5rem', left:'-0.1rem'}}>Login</h4>
                  <AvForm onValidSubmit={(e) => this.handleLogin(e)} className="login-form" method="POST">
                    <AvGroup>
                      <Label for="email">Email</Label>
                      <AvInput required value={this.state.email} onChange={e => this.handleChange(e)} type="text" name="email" id="email" placeholder="Insert Email" />
                    </AvGroup>
                    <AvGroup>
                      <Label for="password">Password</Label>
                      <AvInput
                        required
                        value = {this.state.password}
                        onChange={e => this.handleChange(e)} 
                        type="password"
                        name="password"
                        id="adornment-password"
                        placeholder="Input your password"
                        
                        endAdornment={
                          <InputAdornment position="end">
                            <IconButton
                              aria-label="Toggle password visibility">
                            </IconButton>
                          </InputAdornment>
                        }
                      />
                    </AvGroup>

                    <Row>
                      <Col xs="6" className="text-left">  
                      </Col>
                      <Col xs="6">
                        <Button
                          color="primary"
                          className="float-right px-4"
                          type="submit">
                          Login
                        </Button>
                      </Col>
                    </Row>

                  </AvForm>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default Login
