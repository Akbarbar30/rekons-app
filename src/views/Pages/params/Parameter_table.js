import React from "react";
import MUIDataTable from "mui-datatables"
import { createMuiTheme, MuiThemeProvider, withStyles } from "@material-ui/core/styles"
import { Button } from "reactstrap"
import ParamsAdd from "./Parameter_add";
import ParamsEdit from "./Parameter_edit";

class ParamsTable extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      isDialogOpen: false,
      detailParams: null,
      paramSelected: null
    }
  }

  getMuiTheme = () => createMuiTheme({
    overrides: {
      MUITableHead: {
        root: {
          boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)",
        },
      },
      MUIDataTableHeadCell: {
        root: {
          whiteSpace: 'nowrap',
        },
      },
      MUIDataTableBodyCell: {
        root: {
          whiteSpace: 'nowrap'
        }
      }
    }
  });

  handleDialogOpen = value => {
    this.setState({ isDialogOpen: true });
  };

  handleDialogClose = value => {
    this.setState({ isDialogOpen: false })
  }

  async handleDialogOpenEdit(value) {
    await fetch(`http://rekons.immobisp.com:3005/rekons/params/get/paramsdetail/${value}`)
      .then(detailParamsResp => {
        return detailParamsResp.json()
      })
      .then(detailParams => {
        this.setState({ detailParams: detailParams[0], paramSelected: value, isDialogOpen: true })
        console.log("detailParams", detailParams)
      })
      .catch(error => console.log(error))
    
  }

  async handledelete(value) {

    var form = {
      id_parameter: value,
    }

    console.log(form)

    const url = "http://rekons.immobisp.com:3005/rekons/params/delete"

    await fetch(url, {
      method: "DELETE",
      body: JSON.stringify(form),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        return res.json()
      })
      .then(response => {
        this.setState({ response })
      })
      .catch(error => console.error('Error:', error));

    if (this.state.response.status === 'success') {
      this.setState({
        response: null,
      })
      window.location.reload();
    }
  }

  columns = [
    {
      name: "id",
      id: "id",
      label: "ID",
      options: {
        sort: true
      }
    },
    {
      name: "nama",
      id: "nama",
      label: "Nama",
      options: {
        sort: true
      }
    },
    {
      name: "nilai",
      id: "nilai",
      label: "Nilai (%)",
      options: {
        sort: true
      }
    },
    {
      name: "channel",
      id: "channel",
      label: "Channel",
      options: {
        sort: true
      }
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        customBodyRender: (value) => {
          
          return (
            <div>
              <span style={{ marginLeft: '5px', fontSize: '20px', color: 'blue' }}>
                <i onClick={() => this.handleDialogOpenEdit(value)} className={`far fa-edit`} />
              </span>
              <span style={{ marginLeft: '5px', fontSize: '20px', color: 'red' }}>
                <i onClick={() => this.handledelete(value)} className={`fa fa-trash`} />
              </span>
            </div>
          );
        }
      }
    }
  ];

  data = this.props.params.map(p => [
    parseInt(`${p.id_parameter}`),
    `${p.nama_parameter}`,
    `${p.nilai_parameter*100}`,
    `${p.channel}`,
    parseInt(`${p.id_parameter}`)
  ])

  options = {
    search: false,
    filter: false,
    selectableRows: false,
    print: false,
    filterType: false,
    pagination: true,
    elevation: 0,
    rowsPerPage: 10,
    responsive: 'scroll',
    download: false,
    viewColumns: false
  };

  render() {
    const { detailParams, paramSelected } = this.state
    return (
            <div>
              <MuiThemeProvider theme={this.getMuiTheme()}>
                <MUIDataTable data={this.data} columns={this.columns} options={this.options} />
              </MuiThemeProvider>
              <ParamsEdit
                detailParams={detailParams}
                paramSelected={paramSelected}
                handleDialogClose={this.handleDialogClose}
                isDialogOpen={this.state.isDialogOpen}
              />
            </div>
    );
  }
}

export default ParamsTable