import React, { Component } from "react"
import { Row, Col, FormGroup, Label, Input, Button } from "reactstrap"
import { AvForm, AvField, AvGroup, AvInput, AvFeedback, AvRadioGroup, AvRadio, AvCheckboxGroup, AvCheckbox } from 'availity-reactstrap-validation';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { withStyles } from "@material-ui/core/styles"
import Dialog from "@material-ui/core/Dialog"
import DialogContent from "@material-ui/core/DialogContent"
import MuiDialogTitle from "@material-ui/core/DialogTitle"
import IconButton from "@material-ui/core/IconButton"
import CloseIcon from "@material-ui/icons/Close"
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

const variantIcon = {
  success: CheckCircleIcon,
};

const styles1 = theme => ({
  success: {
    backgroundColor: '#007bff',
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

MySnackbarContent.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);

const styles = theme => ({
  avatar: {
    margin: 10,
    width: 30,
    height: 30
  },
  margin: {
    margin: theme.spacing.unit,
  },
})

const DialogTitle = withStyles(theme => ({
  root: {
    borderBottom: "1px solid #007bff",
    margin: 0,
    padding: 10
  },
  closeButton: {
    position: "absolute",
    right: 5,
    top: 5,
    color: "#007bff"
  }
}))(props => {
  const { children, classes, onClose } = props
  return (
    <MuiDialogTitle disableTypography className={classes.root}>
      <span className="avenir-black-primary ml-4 mb-0" style={{ fontSize: "24px" }}>
        {children}
      </span>
      {onClose ? (
        <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  )
})

export default class Parameter_add extends Component {
  constructor(props) {
    super(props);
    //usahain buat penampung variablenya dlu di state kalo beda folder ya pake props
    this.state = {
        name: '',
        nilai: 0,
        channel: ''
        // response:null,
        // open :false,
        // userSelected : null
        // detailUser : null,
        // listDivisi : null,
        // listRole : null
    };
}

handleChange = (e) => {
  const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
  this.setState({
      [name]: value
  })
}

//fungsi add user
onSubmit = async  (e) => {
 e.preventDefault();
 await this.setState({response : null});

//kan kalo pake postman di bodynya pake json, nah dibuat dlu jsonnya ini caranya
var form = {
        nama_parameter: this.state.name,
        nilai_parameter: this.state.nilai,
        channel: this.state.channel
      }

  //ini urlnya liat di start/route.js , tanya siapa yang buat controllernya
  console.log(form)
  const url = "http://rekons.immobisp.com:3005/rekons/params/add"

  await fetch(url, { method: "POST",
    body: JSON.stringify(form),
    headers: { 
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    } })
    .then(res => {
      return res.json()})
    .then(response => {
      this.setState({response})})
    .catch(error => console.error('Error:', error));
  
    if(this.state.response.status === 'success'){
      this.setState({open:true})
      this.setState ({
        name: '',
        nilai: '',
        channel: ''
      })
      window.location.reload()
    }
  }

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  };

  handleInit() {
    console.log('FilePond instance has initialised', this.pond);
  }

  render() {
    const { isDialogOpen, handleDialogClose } = this.props
    return (
        <Dialog open={isDialogOpen} onClose={handleDialogClose} fullWidth={true} maxWidth="md">
       <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={this.state.open}
        autoHideDuration={6000}
        onClose={this.handleClose}
      >
        <MySnackbarContentWrapper
          onClose={this.handleClose}
          variant="success"
          message="User Created"
        />
      </Snackbar>
      <DialogTitle onClose={handleDialogClose}>Add Parameter</DialogTitle>
      <DialogContent>
      <AvForm onValidSubmit={(e) => this.onSubmit(e)}>
      <Row style={{ margin: "1.5rem 3rem" }} className="add-new-users">
          <Col md="6" style={{ padding: "0.25rem " }}>
            <AvGroup>
              <Label className="avenir-black-primary">Name</Label>
              <AvInput type="text" name="name" value={this.state.name} onChange={e=>this.handleChange(e)} placeholder="Input Name" />
              <AvFeedback>Name required</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label className="avenir-black-primary">Nilai</Label>
              <AvInput type="number" step="0.01" value={this.state.nilai} onChange={e=>this.handleChange(e)} name="nilai" placeholder="Input Nilai" />
              <AvFeedback>Nilai required</AvFeedback>
            </AvGroup>
          </Col>
          <Col md="6" style={{ padding: "0.25rem" }}>
            <AvGroup>
              <Label className="avenir-black-primary">Channel</Label>
              <AvInput type="select" value={this.state.channel} onChange={e=>this.handleChange(e)} name="channel" id="channel-selected">
                <option value="">Select</option>
                <option value="ovo">ovo</option>
                <option value="gopay">gopay</option>
                <option value="dana">dana</option>
                <option value="linkaja">linkaja</option>
                </AvInput>
              <AvFeedback>Channel required</AvFeedback>
            </AvGroup>
            <Button
              type="submit"
              color="primary"
              className="px-4"
              style={{ fontSize: 16, position: "flex", bottom: 0, right: 0, marginTop: "10%" }}      
              >
              Save
            </Button>

          </Col>
        </Row>
      </AvForm>
      </DialogContent>
    </Dialog>
    )
  }
}
