import React from "react"
import ParamsTable from "./Parameter_table"
import CircularProgress from '@material-ui/core/CircularProgress';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    loading: {
        color: '#34B966',
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        width: '5%!important',
        height: '5%!important'
    }
});
class Parameter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            params: [],
            isLoading: false
        }
    }

    async componentDidMount() {
        this.setState({ isLoading: true })
        await fetch('http://rekons.immobisp.com:3005/rekons/params/get/params')
            .then(listResp => {
                return listResp.json()
            })
            .then(listFile => {

                this.setState({ params: listFile, isLoading: false })
            })
            .catch(err => console.log(err))

        console.log(this.state.params)
    }

    render() {
        const { classes } = this.props
        return (
            <>
                {!this.state.isLoading ? (
                    <>
                        <ParamsTable
                            params={this.state.params}
                        />
                    </>
                ) : (
                        <CircularProgress className={classes.loading} />
                    )}

            </>

        )
    }
}

Parameter.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Parameter);

