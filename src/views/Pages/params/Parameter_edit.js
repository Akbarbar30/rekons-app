import React, { Component } from "react"
import { Label, Button } from "reactstrap"
import { AvForm, AvGroup, AvInput, AvFeedback } from 'availity-reactstrap-validation';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { withStyles } from "@material-ui/core/styles"
import Dialog from "@material-ui/core/Dialog"
import DialogContent from "@material-ui/core/DialogContent"
import MuiDialogTitle from "@material-ui/core/DialogTitle"
import IconButton from "@material-ui/core/IconButton"
import CloseIcon from "@material-ui/icons/Close"
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

const variantIcon = {
    success: CheckCircleIcon,
};

const styles1 = theme => ({
    success: {
        backgroundColor: '#34B966',
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.dark,
    },
    icon: {
        fontSize: 20
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing.unit,
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
});

function MySnackbarContent(props) {
    const { classes, className, message, onClose, variant, ...other } = props;
    const Icon = variantIcon[variant];

    return (
        <SnackbarContent
            className={classNames(classes[variant], className)}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                    <Icon className={classNames(classes.icon, classes.iconVariant)} />
                    {message}
                </span>
            }
            action={[
                <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    className={classes.close}
                    onClick={onClose}
                >
                    <CloseIcon className={classes.icon} />
                </IconButton>,
            ]}
            {...other}
        />
    );
}

MySnackbarContent.propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    message: PropTypes.node,
    onClose: PropTypes.func,
    variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: "1px solid #007bff",
        margin: 0,
        padding: 10
    },
    closeButton: {
        position: "absolute",
        right: 5,
        top: 5,
        color: "#007bff"
    }
}))(props => {
    const { children, classes, onClose } = props
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <span className="avenir-black-primary ml-4 mb-0" style={{ fontSize: "24px" }}>
                {children}
            </span>
            {onClose ? (
                <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    )
})

export default class Parameter_edit extends Component {
    constructor(props) {
        super(props);
        //usahain buat penampung variablenya dlu di state kalo beda folder ya pake props
        this.state = {
            open: false
        };
    }

    onSubmit = async (event, values) => {
        event.preventDefault();

        var form = {
            nilai_parameter: parseFloat(values.nilai / 100) ,
            id_parameter: this.props.paramSelected
        }

        console.log(form)

        const url = "http://rekons.immobisp.com:3005/rekons/params/update"

        await fetch(url, {
            method: "PUT",
            body: JSON.stringify(form),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(res => {
                return res.json()
            })
            .then(response => {
                if (response.status === 'success') {
                    this.setState({
                        open: true,
                    })
                    setTimeout(() => { window.location.reload() }, 3000);
                }
            })
            .catch(error => console.error('Error:', error));
    }


    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ open: false });
    };

    render() {
        const { isDialogOpen, handleDialogClose, detailParams } = this.props
        const defaultValues = {
            nilai: detailParams === null || detailParams === "null" ? ' ' : parseFloat(detailParams.nilai_parameter * 100)
        };
        return (
            <>
                {detailParams !== null || detailParams !== {} ? (

                    <Dialog open={isDialogOpen} onClose={handleDialogClose} fullWidth={false} maxWidth="xs">
                        <Snackbar
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={this.state.open}
                            autoHideDuration={6000}
                            onClose={this.handleClose}
                        >
                            <MySnackbarContentWrapper
                                onClose={this.handleClose}
                                variant="success"
                                message="Parameter Edited"
                            />
                        </Snackbar>
                        <DialogTitle onClose={handleDialogClose}>Edit Parameter</DialogTitle>
                        <DialogContent>
                            <AvForm onValidSubmit={this.onSubmit} model={defaultValues} style={{ margin: "1.5rem 3rem" }}>
                                <AvGroup>
                                    <Label className="avenir-black-primary">Nilai (%)</Label>
                                    <AvInput type="number" step="0.1" name="nilai" />
                                    <AvFeedback>Nilai required</AvFeedback>
                                </AvGroup>
                                <Button
                                    type="submit"
                                    color="primary"
                                    className="px-4"
                                    style={{ fontSize: 16 }}
                                >
                                    Save
                                </Button>
                            </AvForm>
                        </DialogContent>
                    </Dialog>

                ) : (
                        <div></div>
                    )}
            </>
        )
    }
}
