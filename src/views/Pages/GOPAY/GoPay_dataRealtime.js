import React from "react"
import MUIDataTable from "mui-datatables"
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles"
import { FormGroup, FormLabel, TextField } from '@material-ui/core';
import moment from "moment";

class GopayRealtime extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            rp: 0,
            tr: 0
        }

        this.handleTableChange = this.handleTableChange.bind(this);
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
            MUITableHead: {
                root: {
                    boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)",
                },
            },
            MUIDataTableHeadCell: {
                root: {
                    whiteSpace: 'nowrap',
                },
            },
            MUIDataTableBodyCell: {
                root: {
                    whiteSpace: 'nowrap'
                }
            },

        }
    });

    handleTableChange = (action, tableState) => {
        var array = []
        array.push(tableState.displayData.map(ts => ts.data.map(d => d)))
        var duit = []

        array[0].map(d => duit.push(d[5]))
        var total_nominal = 0
        this.setState({ tr: duit.length })
        duit.map((item) => {
            total_nominal = item + total_nominal;

        })

        this.setState({ rp: total_nominal })
    };

    columns = [
        {
            name: "NO",
            id: "no",
            label: "ID",
            options: {
                filter: false,
                sort: true
            }
        },
        {
            name: "bill_reff",
            id: "bill_reff",
            label: "Order ID",
            options: {
                filter: false,
                sort: false
            }
        },
        {
            name: "nama_pengirim_gopay",
            id: "nama_pengirim_gopay",
            label: "Nama Pengirim Gopay",
            options: {
                filter: false,
                sort: false
            }
        },
        {
            name: "nama_penerima_gopay",
            id: "nama_penerima_gopay",
            label: "Nama Penerima Gopay",
            options: {
                filter: false,
                sort: false
            }
        },
        {
            name: "bill_date",
            id: "bill_date",
            label: "Bill Date",
            options: {
                filter: true,
                sort: true,
                filterType: 'custom',
                customFilterListRender: v => {
                    if (v['min'] && v['max']) {
                        return `Start Date: ${v['min']}, End Date: ${v['max']}`;
                    } else if (v['min']) {
                        return `Start Date: ${v['min']}`;
                    } else if (v['max']) {
                        return `End Date: ${v['max']}`;
                    }
                    return false;
                },
                filterOptions: {
                    names: [],
                    logic(tanggal, filters) {
                        var check = new Date(tanggal);
                        var from = new Date(filters['min']);
                        var to = new Date(filters['max']);
                        from.setDate(from.getDate());
                        to.setDate(to.getDate());
                        from = new Date(from).setHours(0, 0, 0, 0);
                        to = new Date(to).setHours(23, 59, 59, 59);

                        if (filters['min'] && filters['max'] && check >= to && check <= from) {
                            return true;
                        } else if (filters['min'] && check >= to) {
                            console.log(new Date(check).toString());
                            console.log(new Date(to).toString());
                            return true;
                        } else if (filters['max'] && check <= from) {
                            console.log(new Date(check).toString());
                            console.log(new Date(from).toString());
                            return true;
                        }
                        return false;
                    },
                    display: (filterList, onChange, index, column) => (
                        <div>
                            <FormLabel>Bill Date</FormLabel>
                            <FormGroup row>
                                <TextField
                                    label="Start Date"
                                    type="date"
                                    value={filterList[index]['min'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['min'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%', marginRight: '5%' }}
                                />
                                <TextField
                                    label="End Date"
                                    type="date"
                                    value={filterList[index]['max'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['max'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%' }}
                                />
                            </FormGroup>
                        </div>
                    ),
                },
            },
        },
        {
            name: "total_bayar",
            id: "total_bayar",
            label: "Total Pembayaran",
            options: {
                filter: false,
                sort: true
            },
        },
        {
            name: "nama_penerima",
            id: "nama_penerima",
            label: "Nama Penerima",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "nama_rek",
            id: "nama_rek",
            label: "Nama Rekening",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "bank",
            id: "bank",
            label: "Bank Penerima",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "rek_penerima",
            id: "rek_penerima",
            label: "No Rekening Penerima",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "status",
            id: "status",
            label: "Status",
            options: {
                filter: true,
                sort: false
            },
        }

    ];

    data = this.props.dataRealtime.map((file, index) => [
        parseInt(`${index + 1}`),
        `${file.order_id}`,
        `${file.username_pengirim_gopay === "null" || file.username_pengirim_gopay === null  ? " " : file.username_pengirim_gopay}`,
        `${file.username_penerima_gopay === "null" || file.username_penerima_gopay === null  ? " " : file.username_penerima_gopay}`,
        `${moment(file.transaction_time).format("YYYY-MM-DD HH:mm:ss")}`,
        parseInt(`${file.gross_amount}`),
        `${file.masjid_nama === "null" || file.masjid_nama === null  ? " " : file.masjid_nama}`,
        `${file.masjid_pemilik_rekening === "null" || file.masjid_pemilik_rekening === null ? " " : file.masjid_pemilik_rekening}`,
        `${file.bank_nama === "null" || file.bank_nama === null ? " " : file.bank_nama}`,
        `${file.masjid_no_rekening === "null" || file.masjid_no_rekening === null ? " " : file.masjid_no_rekening}`,
        `${file.transaction_status}`
    ])

    options = {
        filter: true,
        selectableRows: 'none',
        print: false,
        filterType: 'multiselect',
        pagination: true,
        elevation: 0,
        rowsPerPage: 10,
        responsive: 'scroll',
        onTableInit: this.handleTableChange,
        onTableChange: this.handleTableChange,
        download: false
    };

    render() {
        return (
            <>
                {this.props.dataRealtime? (
                    <>
                        <div>
                            <MuiThemeProvider theme={this.getMuiTheme()}>
                                <MUIDataTable title="Data Realtime Muslimpocket" data={this.data} columns={this.columns} options={this.options} />
                                <h4>Total Transaksi = {this.state.tr}</h4>
                                <h4>Total Nominal = Rp. {this.state.rp}</h4>
                            </MuiThemeProvider>


                        </div>
                    </>
                ) : (
                        <div>

                        </div>

                    )}

            </>

        );
    }
}

export default GopayRealtime