import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import UploadCSVTab from './DANA_fileCSV';
import DatabaseTab from './DANA_database';
import RekapBankTab from './DANA_rekapBank';
import ExportCSVTab from './DANA_exportCSV';



function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

const styles = theme => ({
  textField: {
    marginLeft: 0,
    marginRight: 0,
    width: '150px',
    fontFamily: 'Avenir-Black',
    color: '#1890ff',
  },
  dropdown: {
    float: 'right'
  },
  input: {
    height: '10px'
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 100,
    fontFamily: 'Avenir-Black',
    color: '#1890ff',
  },
  root: {
    boxShadow: '0px',
    width: '100%',
  },
  root1: {
    boxShadow: '0px',
    width: '100%',
  },
  tabsIndicator: {
    background: '20px #FFD34B',
    height: '5px',
  },
  tabRoot: {
    textTransform: 'initial',
    fontSize: '20px',
    minWidth: 100,
    color: '#FFFFFF',
    backgroundColor:'#34B966',
    fontFamily: [
      'Avenir-Black',
    ].join(','),
    '&:hover': {
      color: '#FFFFFF',
      opacity: 1,
    },
    '&$tabSelected': {
      fontFamily: 'Avenir-Black',
      color: '#FFFFFF',
      fontWeight:'600'
    },
    '&:focus': {
      fontFamily: 'Avenir-Black',
      color: '#FFFFFF',
    },
  },
  tabSelected: {},
  typography: {
    fontFamily: 'Avenir-Black',
    padding: theme.spacing.unit * 3,
  },
});

class DANAMainPage extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      value : null,
      listFile : [],
      file : null,
      dataDetail : [],
      dataRekap : []
    }
  }

  async componentDidMount() {
    await fetch(`rekons/get/allattachment`)
        .then(listResp => {
            return listResp.json()
        })
        .then( listFile => {

            this.setState({ listFile })
        })
        .catch(err => console.log(err))
}

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleFileChange = async value  =>{
    console.log("ini idnya",value[0])
    this.setState({ file : value[2] });
    await Promise.all([fetch(`/rekons/get/databyattachment/${value[0]}`),fetch(`/rekons/get/datasummary/${value[0]}`)])
      .then(([dataDetailResp,dataRekapResp]) => {
        return Promise.all([dataDetailResp.json(),dataRekapResp.json()])
      })
      .then(([dataDetail,dataRekap]) => {
        this.setState({ dataDetail, dataRekap })
        console.log("dataDetail", dataDetail)
        console.log("dataRekap", dataRekap)
      })
      .catch(error => console.log(error))
  };

  render() {
    const { classes, theme } = this.props;
    const { statusName } = this.state

    return (
        <div className={classes.root}>
          <h4>File in Used : {this.state.file}</h4>
          <AppBar className={classes.root1} position="static" color="none" elevation={1}>

            <Tabs
              value={this.state.value}
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="fullWidth"
              classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
              style={{ fontFamily: 'Avenir-Black' }}
            >
              <Tab
                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                style={{ fontFamily: 'Avenir-Black' }}
                label="File CSV"
              />
              <Tab
                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                style={{ fontFamily: 'Avenir-Black' }}
                label="Database"
              />
               <Tab
                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                style={{ fontFamily: 'Avenir-Black' }}
                label="Rekap Bank"
              />
               <Tab
                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                style={{ fontFamily: 'Avenir-Black' }}
                label="Export"
              />
            </Tabs>

          </AppBar>
          {this.state.value === 0 && <TabContainer><UploadCSVTab handleFileSelect={this.handleFileChange} listFile = {this.state.listFile} /></TabContainer>}
          {this.state.value === 1 && <TabContainer><DatabaseTab dataDetail={this.state.dataDetail}/></TabContainer>}
          {this.state.value === 2 && <TabContainer><RekapBankTab dataRekap={this.state.dataRekap}/></TabContainer>}
          {this.state.value === 3 && <TabContainer><ExportCSVTab dataDetail={this.state.dataDetail}/></TabContainer>}
         
        </div>

    );
  }
}

DANAMainPage.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(DANAMainPage);
