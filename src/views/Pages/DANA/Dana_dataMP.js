import React from "react"
import MUIDataTable from "mui-datatables"
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles"
import { FormGroup, FormLabel, TextField } from '@material-ui/core';
import { Button } from "reactstrap"
import moment from "moment";
import DetailPotongan from "../../../components/dialog/detail_potongan";

class GopayMP extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            rp_awal: 0,
            rp_akhir:0,
            tr: 0,
            isDialogOpenDtPotongan: false,
            detailRekons: null,
        }

        this.handleTableChange = this.handleTableChange.bind(this);
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
            MUITableHead: {
                root: {
                    boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)",
                },
            },
            MUIDataTableHeadCell: {
                root: {
                    whiteSpace: 'nowrap',
                },
            },
            MUIDataTableBodyCell: {
                root: {
                    whiteSpace: 'nowrap'
                }
            },

        }
    });

    handleTableChange = (action, tableState) => {
        var array = []
        array.push(tableState.displayData.map(ts => ts.data.map(d => d)))
        var nominal_awal = []
        var nominal_akhir = []

        array[0].map(d => {
            nominal_awal.push(d[5]);
            nominal_akhir.push(d[7]);
        })
        var total_nominal_awal = 0
        var total_nominal_akhir = 0
        this.setState({ tr: nominal_awal.length })
        nominal_awal.map((item) => {
            total_nominal_awal = item + total_nominal_awal;
        })
        nominal_akhir.map((item) => {
            total_nominal_akhir = item + total_nominal_akhir;
        })
        this.setState({ rp_awal: total_nominal_awal, rp_akhir: total_nominal_akhir })
    };

    handleDialogCloseDtPotongan = value => {
        this.setState({ isDialogOpenDtPotongan: false })
    }

    async handleDialogOpen(value, type) {
        // console.log("edit untuk id : ", value)
        await fetch(`http://rekons.immobisp.com:3005/rekons/${value}`)
            .then(detailRekonsResp => {
                return detailRekonsResp.json()
            })
            .then(detailRekons => {
                this.setState({ detailRekons: detailRekons.result, isDialogOpenDtPotongan: true })
                console.log("detailRekons", detailRekons)
            })
            .catch(error => console.log(error))
    }

    async changeTransferStat(value) {
        const url = "http://rekons.immobisp.com:3005/rekons/" + value

        await fetch(url, {
            method: "PUT",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(res => {
                return res.json()
            })
            .then(response => {
                if (response.status === 'success') {
                    alert("Status Transfer terupdate")
                    setTimeout(() => { window.location.reload() }, 3000);
                }
            })
            .catch(error => console.error('Error:', error));
    }

    columnsMP = [
        {
            name: "NO",
            id: "no",
            label: "ID",
            options: {
                filter: false,
                sort: true
            }
        },
        {
            name: "reference_id",
            id: "reference_id",
            label: "Reference ID",
            options: {
                filter: false,
                sort: false
            }
        },
        {
            name: "tgl_bayar",
            id: "tgl_bayar",
            label: "Tanggal Bayar",
            options: {
                filter: true,
                sort: true,
                filterType: 'custom',
                customFilterListRender: v => {
                    if (v['min'] && v['max']) {
                        return `Start Date: ${v['min']}, End Date: ${v['max']}`;
                    } else if (v['min']) {
                        return `Start Date: ${v['min']}`;
                    } else if (v['max']) {
                        return `End Date: ${v['max']}`;
                    }
                    return false;
                },
                filterOptions: {
                    names: [],
                    logic(tanggal, filters) {
                        var check = new Date(tanggal);
                        var from = new Date(filters['min']);
                        var to = new Date(filters['max']);
                        from.setDate(from.getDate());
                        to.setDate(to.getDate());
                        from = new Date(from).setHours(0, 0, 0, 0);
                        to = new Date(to).setHours(23, 59, 59, 59);

                        if (filters['min'] && filters['max'] && check >= to && check <= from) {
                            return true;
                        } else if (filters['min'] && check >= to) {
                            console.log(new Date(check).toString());
                            console.log(new Date(to).toString());
                            return true;
                        } else if (filters['max'] && check <= from) {
                            console.log(new Date(check).toString());
                            console.log(new Date(from).toString());
                            return true;
                        }
                        return false;
                    },
                    display: (filterList, onChange, index, column) => (
                        <div>
                            <FormLabel>Tanggal Pembayaran</FormLabel>
                            <FormGroup row>
                                <TextField
                                    label="Start Date"
                                    type="date"
                                    value={filterList[index]['min'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['min'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%', marginRight: '5%' }}
                                />
                                <TextField
                                    label="End Date"
                                    type="date"
                                    value={filterList[index]['max'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['max'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%' }}
                                />
                            </FormGroup>
                        </div>
                    ),
                },
            },
        },
        {
            name: "sender",
            id: "sender",
            label: "Username Pengirim",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "receiver",
            id: "receiver",
            label: "Username Penerima",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "nominal_awal",
            id: "nominal_awal",
            label: "Nominal Awal",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "nominal_potongan",
            id: "nominal_potongan",
            label: "Nominal Potongan",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "nominal_akhir",
            id: "nominal_akhir",
            label: "Nominal Akhir",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "nama_rek",
            id: "nama_rek",
            label: "Nama Rekening",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "bank",
            id: "bank",
            label: "Bank Penerima",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "rek_penerima",
            id: "rek_penerima",
            label: "No Rekening Penerima",
            options: {
                filter: true,
                sort: false,
                filterOptions: {
                    names: ['Tidak Lengkap', 'Lengkap'],
                    logic(rek_penerima, filterVal) {
                        const show =
                            (filterVal.indexOf('Tidak Lengkap') >= 0 && (rek_penerima === "" && rek_penerima === " ")) ||
                            (filterVal.indexOf('Lengkap') >= 0 && (rek_penerima !== "" && rek_penerima !== " "));
                        return !show;
                    },
                },
            },
        },
        {
            name: "status",
            id: "status",
            label: "Status",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "isTransfer",
            id: "isTransfer",
            label: "Transfer Bank",
            options: {
                filter: true,
                sort: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    if (value === "T") {
                        return (
                            <i className={`fas fa-check`} style={{ color: 'green' }} />
                        )
                    } else if (value === "F") {
                        return <div></div>
                    }
                },
                filterOptions: {
                    names: ['Belum Transfer', 'Sudah Transfer'],
                    logic(salary, filterVal) {

                        const show =
                            (filterVal.indexOf('Belum Transfer') >= 0 && salary === "F") ||
                            (filterVal.indexOf('Sudah Transfer') >= 0 && salary === "T");
                        return !show;
                    },
                },
            },
        },
        {
            name: "action",
            id: "action",
            label: "Action",
            options: {
                filter: false,
                sort: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    return (
                        <div>
                            <span>
                                <Button color="primary" className="btn-add-new" size="sm" onClick={() => this.handleDialogOpen(value[0], "potongan")}>
                                    Detail Potongan
                                </Button>
                            </span>
                            {value[1] === "F" &&
                                <span>
                                    <Button color="primary" className="btn-add-new" size="sm" onClick={() => this.changeTransferStat(value[0])}>
                                        Done Transfer
                                </Button>
                                </span>}
                        </div>

                    )
                }
            }
        }
    ];

    dataMP = this.props.dataMP.map((file, index) => [
        parseInt(`${index + 1}`),
        `${file.reference_id}`,
        `${moment(file.tgl_pembayaran).format("YYYY-MM-DD HH:mm:ss")}`,
        `${file.sender}`,
        `${file.receiver}`,
        parseInt(`${file.total_pembayaran}`),
        parseFloat(`${file.nominal_potongan_kmdn + file.nominal_potongan_channel + file.nominal_potongan_cashback}`),
        parseFloat(`${file.total_pembayaran - (file.nominal_potongan_kmdn + file.nominal_potongan_channel + file.nominal_potongan_cashback)}`),
        `${file.nama_rekening_penerima === "null" ? " " : file.nama_rekening_penerima}`,
        `${file.bank_penerima === "null" ? " " : file.bank_penerima}`,
        `${file.no_rekening_penerima === "null" ? " " : file.no_rekening_penerima}`,
        `${file.status}`,
        `${file.isTransfer}`,
        [`${file.id}`, `${file.isTransfer}`]
    ])

    optionsMP = {
        filter: true,
        selectableRows: 'none',
        print: false,
        filterType: 'multiselect',
        pagination: true,
        elevation: 0,
        rowsPerPage: 10,
        responsive: 'scroll',
        onTableInit: this.handleTableChange,
        onTableChange: this.handleTableChange,
        download: false
    };

    render() {
        return (
            <>
                {this.props.dataMP ? (
                    <>
                        <div>
                            <MuiThemeProvider theme={this.getMuiTheme()}>
                                <MUIDataTable title="Data Muslimpocket" data={this.dataMP} columns={this.columnsMP} options={this.optionsMP} />
                                <h4>Total Transaksi = {this.state.tr}</h4>
                                <h4>Total Nominal Awal = Rp. {this.state.rp_awal}</h4>
                                <h4>Total Potongan = Rp. {(this.state.rp_awal - this.state.rp_akhir.toFixed(1)).toFixed(1)}</h4>
                                <h4>Total Nominal Akhir = Rp. {this.state.rp_akhir.toFixed(1)}</h4>
                            </MuiThemeProvider>
                            <DetailPotongan
                                detailDataRek={this.state.detailRekons}
                                handleDialogClose={this.handleDialogCloseDtPotongan}
                                isDialogOpen={this.state.isDialogOpenDtPotongan}
                            />
                        </div>
                    </>
                ) : (
                        <div>

                        </div>

                    )}

            </>

        );
    }
}

export default GopayMP