import React from "react"
import { Row, Col, FormGroup, Label, Input, Button } from "reactstrap"
import { withStyles } from "@material-ui/core/styles"
import Dialog from "@material-ui/core/Dialog"
import DialogContent from "@material-ui/core/DialogContent"
import MuiDialogTitle from "@material-ui/core/DialogTitle"
import IconButton from "@material-ui/core/IconButton"
import CloseIcon from "@material-ui/icons/Close"
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import FilePondPluginFileEncode from 'filepond-plugin-file-encode';
import FilePondPluginImageCrop from 'filepond-plugin-image-crop';
import FilePondPluginImageResize from 'filepond-plugin-image-resize';
import FilePondPluginImageTransform from 'filepond-plugin-image-transform';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';

registerPlugin(FilePondPluginImageTransform,FilePondPluginImageResize,FilePondPluginImageCrop,FilePondPluginFileEncode,FilePondPluginFileValidateType,FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

const DialogTitle = withStyles(theme => ({
  root: {
    borderBottom: "1px solid #34B966",
    margin: 0,
    padding: 10
  },
  closeButton: {
    position: "absolute",
    right: 5,
    top: 5,
    color: "#34B966"
  }
}))(props => {
  const { children, classes, onClose } = props
  return (
    <MuiDialogTitle disableTypography className={classes.root}>
      <span className="avenir-black-primary ml-4 mb-0" style={{ fontSize: "24px" }}>
        {children}
      </span>
      {onClose ? (
        <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  )
})

export default class ConvertCSVtoDBDANA extends React.Component {
  state = {
        file: null,
        open :false
  }

  handleInit() {
    console.log('FilePond instance has initialised', this.pond);
  }

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      window.location.reload();
    }

   window.location.reload()
  };

  render() {
    const { isDialogOpen, handleDialogClose,onClose } = this.props
    const { attachment } = this.state
    return (
      <Dialog open={isDialogOpen} onClose={handleDialogClose} fullWidth={true} maxWidth="md">
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          action={[
            <Button key="undo" color="primary" size="small" onClick={this.handleClose}>
              UNDO
            </Button>
          ]}
          message={<span id="message-id">Convert Into Database Success</span>}
          open={this.state.open}
          autoHideDuration={6000}
          onClose={this.handleClose}
        >
        </Snackbar>
        <DialogTitle onClose={handleDialogClose}>Convert CSV/XLSX</DialogTitle>
        <DialogContent style ={{padding : '20px 80px 25px'}}>
        <Col md="12" style={{ padding: "0.25rem" }}>
            <FormGroup>
              <Label className="avenir-black-primary">Upload XLSX/CSV</Label>
              <FilePond ref={ref => this.pond = ref}
                          className = "file-faspay"
                          files={this.state.file1}
                          instantUpload = {false}
                          acceptedFileTypes = {
                            ['text/x-csv', 
                            'application/csv',
                            'text/csv',
                            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 
                            'application/vnd.ms-excel']
                          }
                          server={{
                            process: {
                              url: "rekons/upload",
                              method: 'POST',
                              onload: (response) => {
                                console.log(response)
                                this.setState({open:true})
                              },
                            },
                          }}
                          name = "file"
                          allowMultiple={false}
                          oninit={() => this.handleInit() }
                          onupdatefiles={ async (fileItems) => {
                              // Set current file objects to this.state
                              await this.setState({
                                file1: fileItems.map(fileitem=> fileitem.file)
                              });

                              if(this.state.file1.length !== 0)
                                console.log("ini DANA", this.state.file1)
                          }}>
                </FilePond>
            </FormGroup>
        </Col>
        </DialogContent>
      </Dialog>
    )
  }
}
