import React from "react";
// react plugin for creating charts
// import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
// import Danger from "../../components/Typography/Danger.js";
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardIcon from "../../components/Card/CardIcon.js";
import CardFooter from "../../components/Card/CardFooter.js";

// import { bugs, website, server } from "variables/general.js";

import styles from "./dashboardStyle.js";

const useStyles = makeStyles(styles);

export default function DashboardCard(props) {
  const classes = useStyles();
  return (
    <div>
          <Card>
            <CardHeader color="success" stats icon>
              <CardIcon color="success">
                {props.channel}
              </CardIcon>
              <p className={classes.cardCategory}>Total Nominal Akhir</p>
              <h3 className={classes.cardTitle}>
                {props.total_akhir}
              </h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <GridContainer>
                    <GridItem sm={6}>
                        Jumlah Transaksi
                    </GridItem>
                    <GridItem sm={6} align="right">
                        {props.jumlah_transaksi}
                    </GridItem>
                    <GridItem sm={6} >
                        Total Nominal Awal
                    </GridItem>
                    <GridItem sm={6} align="right">
                        {props.total_awal}
                    </GridItem>
                    <GridItem sm={6}>
                        Total Potongan KMDN
                    </GridItem>
                    <GridItem sm={6} align="right">
                        {props.potongan_kmdn}
                    </GridItem>
                    <GridItem sm={6}>
                        Total Potongan Channel
                    </GridItem>
                    <GridItem sm={6} align="right">
                        {props.potongan_channel}
                    </GridItem>
                    <GridItem sm={6}>
                        Total Rejeki Poin
                    </GridItem>
                    <GridItem sm={6} align="right">
                       {props.potongan_poin}
                    </GridItem>
                </GridContainer>
              </div>
            </CardFooter>
          </Card>
    </div>
  );
}
