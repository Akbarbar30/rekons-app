import React, { Component, lazy, Suspense } from "react"

import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import CircularProgress from "@material-ui/core/CircularProgress"
const DashboardCard = lazy(() => import("./DashboardCard"))
const DashboardCardTop = lazy(() => import("./DashboardCardTop"))
const DashboardCardTable = lazy(() => import("./DashboardTable"))

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataDashboard: [],
      dataUser: [],
      dataMasjid: [],
      isLoading: false
    }

    this.renderCard = this.renderCard.bind(this);
  }

  async componentDidMount() {
    this.setState({ isLoading: true })
    await Promise.all(
      [
        fetch(`http://rekons.immobisp.com:3005/rekons/dashboard/alldata`, { method: 'GET' })
      ]
    ).then(([dataRekonsResp]) => {
      return Promise.all([dataRekonsResp.json()])
    }).then(([dataRekons]) => {
      this.setState({
        dataDashboard: dataRekons.data,
        dataMasjid: dataRekons.list_top_masjid,
        dataUser: dataRekons.list_top_user,
        isLoading: false
      })
    }).catch(error => console.log(error));
    console.log(this.state.dataDashboard)
  }

  renderCard(d) {
    var formatter = new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR"
    });

    if (d.Channel === "all") {
      return (
        <GridItem xs={12} sm={6} md={12}><DashboardCardTop
          channel={d.Channel}
          total_akhir={formatter.format(d.Total_Seluruh_Nominal_Akhir)}
          jumlah_transaksi={d.Total_Seluruh_Transaksi}
          total_awal={formatter.format(d.Total_Nominal_Seluruh_Transaksi)}
          potongan_kmdn={formatter.format(d.Total_Nominal_Potongan_KMDN)}
          potongan_channel={formatter.format(d.Total_Nominal_Potongan_Channel)}
          potongan_poin={formatter.format(d.Total_Rejeki_Poin)} />
        </GridItem>
      )
    } else {
      return (
        <GridItem xs={12} sm={6} md={3}><DashboardCard
          channel={d.Channel}
          total_akhir={formatter.format(d.Total_Seluruh_Nominal_Akhir)}
          jumlah_transaksi={d.Total_Seluruh_Transaksi}
          total_awal={formatter.format(d.Total_Nominal_Seluruh_Transaksi)}
          potongan_kmdn={formatter.format(d.Total_Nominal_Potongan_KMDN)}
          potongan_channel={formatter.format(d.Total_Nominal_Potongan_Channel)}
          potongan_poin={formatter.format(d.Total_Rejeki_Poin)} />
        </GridItem>
      )
    }
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {
    const { isLoading, dataDashboard, dataMasjid, dataUser } = this.state;
    var formatter = new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR"
    });
    return (
      <div className="animated fadeIn">
        <Suspense fallback={this.loading()}>
          {!isLoading ? (
            <div>
              <GridContainer>
                {dataDashboard.map(d => this.renderCard(d))}
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={6} md={6}>
                  <DashboardCardTable
                    title={"Top 10 Masjid"}
                    subtitle={"10 Masjid dengan nominal infaq terbanyak"}
                    tableHead={["No", "Nama Masjid", "Jumlah Transaksi", "Total Nominal"]}
                    tableData={dataMasjid.map((d, i) => [i + 1, `${d.nama_penerima}`, `${d.jumlah_transaksi}`, `${formatter.format(d.nominal_transaksi_awal)}`])}
                  />
                </GridItem>
                <GridItem xs={12} sm={6} md={6}>
                  <DashboardCardTable
                    title={"Top 10 Penginfaq"}
                    subtitle={"10 Penginfaq dengan nominal infaq terbanyak"}
                    tableHead={["No", "Nama User", "Jumlah Transaksi", "Total Nominal"]}
                    tableData={dataUser.map((d, i) => [i + 1, `${d.sender}`, `${d.jumlah_transaksi}`, `${formatter.format(d.nominal_transaksi_awal)}`])}
                  />
                </GridItem>
              </GridContainer>
            </div>


          ) : (
              <CircularProgress style={{ marginLeft: 10, color: "#34B966" }} />
            )}



        </Suspense>
      </div>
    )
  }
}

export default Dashboard
