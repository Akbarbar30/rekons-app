import React from "react";
import { makeStyles } from "@material-ui/core/styles";
// core components
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardBody from "../../components/Card/CardBody.js";
import Table from "../../components/Table/Table.js";

import styles from "./dashboardStyle.js";

const useStyles = makeStyles(styles);

export default function DashboardCard(props) {
  const classes = useStyles();
  return (
    <div>
          <Card>
            <CardHeader color="success">
              <h4 className={classes.cardTitleWhite}>{props.title}</h4>
              <p className={classes.cardCategoryWhite}>
                {props.subtitle}
              </p>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="success"
                tableHead={props.tableHead}
                tableData={props.tableData}
              />
            </CardBody>
          </Card>
    </div>
  );
}
