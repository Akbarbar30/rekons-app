import React from "react";
// react plugin for creating charts
// import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
// import Danger from "../../components/Typography/Danger.js";
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardIcon from "../../components/Card/CardIcon.js";
import CardFooter from "../../components/Card/CardFooter.js";

// import { bugs, website, server } from "variables/general.js";

import styles from "./dashboardStyle.js";

const useStyles = makeStyles(styles);

export default function DashboardCard(props) {
    const classes = useStyles();
    return (
        <div>
            <Card>
                <CardHeader color="success" stats icon>
                    <CardIcon color="success">
                        {props.channel}
                    </CardIcon>
                    <GridContainer>
                        <GridItem sm={2}>
                            <p className={classes.cardCategory}>Jumlah Transaksi</p>
                            <h4 className={classes.cardTitle}>
                                {props.jumlah_transaksi}
                            </h4>
                        </GridItem>
                        <GridItem sm={2}>
                            <p className={classes.cardCategory}>Total Nominal Awal</p>
                            <h4 className={classes.cardTitle}>
                                {props.total_awal}
                            </h4>
                        </GridItem>
                        <GridItem sm={2}>
                            <p className={classes.cardCategory}>Total Potongan KMDN</p>
                            <h4 className={classes.cardTitle}>
                                {props.potongan_kmdn}
                            </h4>
                        </GridItem>
                        <GridItem sm={2}>
                            <p className={classes.cardCategory}>Total Potongan Channel</p>
                            <h4 className={classes.cardTitle}>
                                {props.potongan_channel}
                            </h4>
                        </GridItem>
                        <GridItem sm={2}>
                            <p className={classes.cardCategory}>Total Rejeki Poin</p>
                            <h4 className={classes.cardTitle}>
                                {props.potongan_poin}
                            </h4>
                        </GridItem>
                        <GridItem sm={2}>
                            <p className={classes.cardCategory}>Total Nominal Akhir</p>
                            <h4 className={classes.cardTitle}>
                                {props.total_akhir}
                            </h4>
                        </GridItem>
                    </GridContainer>
                </CardHeader>
                <CardFooter stats>
                    <div className={classes.stats}>
                        All Time
              </div>
                </CardFooter>
            </Card>
        </div>
    );
}
