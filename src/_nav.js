export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-home"
    },
    {
      name: "OVO",
      url: "/ovo",
      icon: "fas fa-bullseye"
    },
    {
      name: "GOPAY",
      url: "/gopay",
      icon: "fas fa-wallet",
    },
    {
      name: "LinkAja",
      url: "/linkaja",
      icon: "far fa-money-bill-alt"
    },
    {
      name: "DANA",
      url: "/dana",
      icon: "fas fa-money-bill-wave-alt",
    },
    {
      name: "Parameter",
      url: "/params",
      icon: "fas fa-keyboard", 
    },
    {
      name: "Rekap Transaksi",
      url: "/rekap",
      icon: "fas fa-coins", 
    },
    {
      name: "Export Transaksi",
      url: "/exportcsv",
      icon: "fas fa-file", 
    }

  ]
}
