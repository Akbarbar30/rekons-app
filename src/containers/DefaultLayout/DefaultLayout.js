import React, { Component, Suspense } from "react"
import { Redirect, Route, Switch } from "react-router-dom"
import { Container, Alert } from "reactstrap"
import { Offline, Online } from 'react-detect-offline'

import {
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav
} from "@coreui/react"
// sidebar nav config
import nav from "../../_nav"
// routes config
import routes from "../../routes"
const DefaultHeader = React.lazy(() => import("./DefaultHeader"))

class DefaultLayout extends Component {

  state = {
    user: []
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  async signOut(e) {
    e.preventDefault()
    await localStorage.removeItem('token');
    await localStorage.removeItem('user');
    this.props.history.push("/login")
  }

  async componentDidMount() {
    if (!localStorage.getItem('user')) {
      await localStorage.removeItem('token');
      await localStorage.removeItem('user');
      return this.props.history.push("/login");

    } else {
      var users = await JSON.parse(localStorage.getItem("user"))
      await this.setState({ user: users });
      console.log("ini hasil login", this.state.user)

    }
    console.log(this.props)

  }

  render() {
    // Configuring names for each main rendered component
    const route = routes.find(loc => loc.path === this.props.location.pathname)
    console.log("ini locnya", routes.find(loc => loc.path === this.props.location.pathname))
    console.log("ini pathname", this.props.location.pathname)
    console.log("ini", route)
    const heading = route.name

    return (
      <div className="app">
        <AppHeader fixed>
          <Suspense fallback={this.loading()}>
            <DefaultHeader onLogout={e => this.signOut(e)} />
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarMinimizer />
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
              <AppSidebarNav navConfig={nav} {...this.props} />
            </Suspense>
            <AppSidebarFooter />
          </AppSidebar>
          <main className="main">
            <Offline>
              <Alert color="danger">
                Offline Mode
              </Alert>
            </Offline>
            <h2
              className="avenir-black-primary"
              style={{
                marginTop: 20,
                marginBottom: 20,
                marginLeft: 30,
                display: "inline-block"
              }}>
              {heading}
            </h2>
            <Container fluid>
              <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => <route.component {...props} />}
                      />
                    ) : null
                  })}
                  <Redirect from="/" to="/dashboard" />
                </Switch>
              </Suspense>
            </Container>
          </main>
        </div>

      </div>

    )
  }
}

export default DefaultLayout
