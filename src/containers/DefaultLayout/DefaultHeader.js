import React, { Component } from "react"
import { Link} from "react-router-dom"
import {
  Nav,
  NavItem
} from "reactstrap"
import PropTypes from "prop-types"

import {
  AppSidebarToggler
} from "@coreui/react"
import logo from "../../assets/img/brand/logo.png"
import { withStyles } from "@material-ui/core/styles"

const propTypes = {
  children: PropTypes.node
}

const defaultProps = {}

const styles = theme => ({
  avatar: {
    margin: 10,
    width: 30,
    height: 30
  }
})

class DefaultHeader extends Component {

  render() {
    // eslint-disable-next-line
    const { classes,children, ...attributes } = this.props

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-md-none" display="md" mobile />
        <img
          src={logo}
          alt="Immobi Tracker Logo"
          style={{ width: 'auto', height: 35, marginLeft: 30 }}
        />
        <Nav className="ml-auto" navbar>
          <NavItem style={{ marginRight: '4rem',marginTop: 10}}>
            <Link className="nav-link1" activeClassName="actives" to="">
              <h6 className="avenir-black-primary navbar-item" onClick={e => this.props.onLogout(e)}>Sign Out</h6>
            </Link>
          </NavItem>
        </Nav>
      </React.Fragment>
    )
  }
}

DefaultHeader.propTypes = propTypes
DefaultHeader.defaultProps = defaultProps

export default withStyles(styles)(DefaultHeader)
