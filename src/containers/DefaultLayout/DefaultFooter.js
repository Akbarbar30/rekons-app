import React, { Component } from "react"
import PropTypes from "prop-types"

const propTypes = {
  children: PropTypes.node
}

const defaultProps = {}

class DefaultFooter extends Component {
  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props

    return (
      <React.Fragment>
        Footer
        {/*
         <span>
          &copy; 2019 <a href="http://www.immobisp.com/">PT Immobi Solusi Prima </a>
        </span>
        <span className="ml-auto">
          Powered by <a href="https://www.linkedin.com/in/hafizulihsanfadli/">Deyen</a>{" "}
          <a href="https://www.linkedin.com/in/ruslan-uchan/">Uchan</a>
        </span>*/}
      </React.Fragment>
    )
  }
}

DefaultFooter.propTypes = propTypes
DefaultFooter.defaultProps = defaultProps

export default DefaultFooter
