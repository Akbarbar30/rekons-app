import React, { Component } from "react"
import { Row, Col, Label, Input, FormGroup, Button } from "reactstrap"

import "./MyAccount.scss"
import UserImg from "../../assets/img/my-account/user@2x.png"
import CameraImg from "../../assets/img/my-account/photo-camera@2x.png"

export default class MyAccount extends Component {
  /*
   TODO:    Add image functionality by clicking the camera img
   TODO:    Add on submit event and on-front validation
   @state   isFilled -> if data already exists, convert button to edit
   @                    else, convert button text to save
   @method  validate input
   @        upload image
   @        POST new profile
  */

 constructor(props) {
  super(props);
  //usahain buat penampung variablenya dlu di state kalo beda folder ya pake props
  this.state = {
     user : null , 
     name : null ,
     password : null ,
     email : null,
     phone : null,
     culture : null,
     isEditing : false,
     photo : null
  };
}

 async componentDidMount() {
  var users = await JSON.parse(localStorage.getItem("user"));
  await fetch("api/v1/user/myaccount/"+ users.id)
    .then(resp => {
      console.log(resp.status, resp.statusText)
      return resp.json()
    })
    .then(users =>{
      this.setState({user : users[0][0]})
    })
    .catch(err => console.log(err))

  await this.setState({name : this.state.user.name, culture : this.state.user.culture , email : this.state.user.email, phone : this.state.user.phone, photo : this.state.user.photo})
  console.log("ini hasil login", this.state.user)
}


  handleAddImage = () => {
    console.log("Add Image Handler Called")
  }

  handleClickEdit = async (e) => {
    this.setState({isEditing : true})
  }

  handleClickSave = async (e)=> {

    e.preventDefault();
    var form = {
      userId : parseInt(this.state.user.id),
      Name : this.state.name,
      Email : this.state.email,
      Culture : this.state.culture,
      Phone : this.state.phone,
      password : this.state.password
    }

    const url = "api/v1/user/myaccount/update"

  await fetch(url, { method: "POST",
    body: JSON.stringify(form),
    headers: { 
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    } })
    .then(res => {
      return res.json()})
    .then(response => {
      this.setState({response})})
    .catch(error => console.error('Error:', error));
  
    if(this.state.response.status === 'success'){
      
      this.setState ({
        name: '',
        password: '',
        email: '',
        phone: '',
        culture: "",
        response:null,
      })
      window.location.reload();
    }

    console.log(form)
    this.setState({isEditing : false})
  }
  
  handleChange = (e) => {
    const target = e.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
    this.setState({
        [name]: value
    })
  }

  generateBody = () => {
    if(this.state.isEditing === false){
      return(
        <React.Fragment>
        <Row>
            <Col lg="2">
            <FormGroup>
              <img src={`http://tracker.immobisp.com/avatar/${this.state.photo}`} alt="User" />
              <img
                style={{
                  position: "relative",
                  marginTop: "50%",
                  marginLeft: "-10%",
                  cursor: "pointer"
                }}
                src={CameraImg}
                alt="Camera"
                onClick={this.handleAddImage}
              />
            </FormGroup>
          </Col>
          <Col lg="4">
            <FormGroup>
              <Label className="avenir-black-primary" for="fullname">
                Fullname
              </Label>
              <Input disabled value ={this.state.name} type="text" name="fullname" id="fullname" placeholder="Input Full Name" />
            </FormGroup>
            <FormGroup style={{ marginTop: "20%" }}>
              <Label className="avenir-black-primary" for="language">
                Language
              </Label>
              <Input disabled value ={this.state.culture} type="select" name="culture" id="language">
              <option value = "" >Select Language</option>
              <option value = "id" >Indonesia</option>
              <option value = "en-us" >English (US)</option>
              <option value = "en-uk" >English (UK)</option>
              </Input>
            </FormGroup>
          </Col>
          <Col lg="4">
            <FormGroup>
              <Label className="avenir-black-primary" for="email">
                Email
              </Label>
              <Input disabled value ={this.state.email} type="email" name="email" id="email" placeholder="Input email" />
            </FormGroup>
            <FormGroup style={{ marginTop: "20%" }}>
              <Label className="avenir-black-primary" for="phone">
                Phone
              </Label>
              <Input disabled value ={this.state.phone} type="text" name="phone" id="phone" placeholder="Input a phone number" />
            </FormGroup>
            <Button
              color="primary"
              className="px-4"
              onClick={e=>this.handleClickEdit(e)}
              style={{ fontSize: 18, position: "absolute", right: 0 }}>
              Edit
            </Button>
          </Col>
          <Col lg="2" />
        </Row>
      </React.Fragment>
      )
      
    }else{
      return(
        <React.Fragment>
        <Row>
            <Col lg="2">
            <FormGroup>
              <img src={UserImg} alt="User" />
              <img
                style={{
                  position: "relative",
                  marginTop: "50%",
                  marginLeft: "-10%",
                  cursor: "pointer"
                }}
                src={CameraImg}
                alt="Camera"
                onClick={this.handleAddImage}
              />
            </FormGroup>
          </Col>
          <Col lg="4">
            <FormGroup>
              <Label className="avenir-black-primary" for="name">
                Fullname
              </Label>
              <Input value ={this.state.name} onChange={e=>this.handleChange(e)} type="text" name="name" id="name" placeholder="Input Full Name" />
            </FormGroup>
            <FormGroup style={{ marginTop: "20%" }}>
              <Label className="avenir-black-primary" for="language">
                Language
              </Label>
              <Input value ={this.state.culture} onChange={e=>this.handleChange(e)} type="select" name="culture" id="language">
              <option value = "" >Select Language</option>
              <option value = "id" >Indonesia</option>
              <option value = "en-us" >English (US)</option>
              <option value = "en-uk" >English (UK)</option>
              </Input>
            </FormGroup>
          </Col>
          <Col lg="4">
            <FormGroup>
              <Label className="avenir-black-primary" for="email">
                Email
              </Label>
              <Input value ={this.state.email} onChange={e=>this.handleChange(e)} type="email" name="email" id="email" placeholder="Input email" />
            </FormGroup>
            <FormGroup style={{ marginTop: "20%" }}>
              <Label className="avenir-black-primary" for="phone">
                Phone
              </Label>
              <Input value ={this.state.phone} onChange={e=>this.handleChange(e)} type="text" name="phone" id="phone" placeholder="Input a phone number" />
            </FormGroup>
            <FormGroup style={{ marginTop: "20%" }}>
              <Label className="avenir-black-primary" for="password">
                New Password
              </Label>
              <Input value ={this.state.password} onChange={e=>this.handleChange(e)} type="text" name="password" id="password" placeholder="Input a new password" />
            </FormGroup>
            <Button
              color="primary"
              className="px-4"
              onClick={e=>this.handleClickSave(e)}
              style={{ fontSize: 18, position: "absolute", right: 0 }}>
              Save
            </Button>
          </Col>
          <Col lg="2" />
          </Row>
        </React.Fragment>
      )
    }

  }

  render() {
    return (
    this.generateBody()    
    )
  }
}
