import { Form, DatePicker, Button } from 'antd';
import React from "react"
const { RangePicker } = DatePicker;

class TimeRelatedForm extends React.Component {
    
    handleSubmit = e => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {
            if (err) {
                return;
            }

            const rangeTimeValue = fieldsValue['range-time-picker'];
            const values = {
                'start-date': rangeTimeValue[0].format('YYYY-MM-DD HH:mm:ss'),
                'end-date': rangeTimeValue[1].format('YYYY-MM-DD HH:mm:ss'),
            };
            console.log('Received values of form: ', values);

            this.props.onSubmit(rangeTimeValue[0].format('YYYY-MM-DD HH:mm:ss'),rangeTimeValue[1].format('YYYY-MM-DD HH:mm:ss'))
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 10 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 20 },
                sm: { span: 10, offset: 1 },
            },
        };
        const rangeConfig = {
            rules: [{ type: 'array', required: true, message: 'Please select time!' }],
        };
        return (
            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                <Form.Item wrapperCol={{
                    xs: { span: 24, offset: 0 },
                    sm: { span: 16, offset: 0 },
                }} label="Periode Tanggal Pembayaran">
                    {getFieldDecorator('range-time-picker', rangeConfig)(
                        <RangePicker showTime format="YYYY-MM-DD HH:mm:ss" />,
                    )}
                    <Button style={{ marginLeft: 10, borderRadius: 20, backgroundColor: "#34B966" }} type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

const WrappedTimeRelatedForm = Form.create({ name: 'time_related_controls' })(TimeRelatedForm);

export default WrappedTimeRelatedForm;