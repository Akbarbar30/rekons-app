import React from "react"
import { Card, CardBody, CardHeader, Col, Progress } from "reactstrap"
import { Link, withRouter } from 'react-router-dom';
import "./DashboardCard.scss"

const DashboardCard = props => {
  /*
   TODO: props
   @f    komponen ini akan diiterasi (map) oleh komponen dashboard
   */

  const setID = async (event,projectID) => {
  
    await localStorage.setItem("id",projectID);
    props.history.push("/projectsDetail");
  }


  const colorCode = ["blue", "green", "yellow", "black", "pink", "red"]
  const currentColor = colorCode[props.idx % 6]

  return (
    <React.Fragment>
      <Col xs="12" sm="6" md="4">
        <Card className={`dashboard-card dc-${currentColor}`} onClick={() => console.log(props.id)}>
        
          <CardHeader className="p-3"><Link onClick={e => setID(e,props.id)} to="">
            <h5 className="avenir-black-primary">{props.name}</h5>{" "}</Link>
            <span className="dashboard-card-status">{props.status}</span>
            <div className="dashboard-card-status">Progress</div>
            <div className="dashboard-card-status">
              <Progress value={props.total_task == null? 0 :(((props.total_done_task + props.total_complete_task)/props.total_task)*100).toFixed(2) }>{props.total_task== null? 0.00.toFixed(2) :(((props.total_done_task + props.total_complete_task)/props.total_task)*100).toFixed(2) }%</Progress>
            </div>
          </CardHeader>
          <CardBody>
            <h6 className="dashboard-card-id">ID : {props.id}</h6>
            <div className="dashboard-card-body">
              <span className="dashboard-card-items">
                Created by <br />
                {props.creator}
              </span>
              <span className="dashboard-card-items">
                Type <br />
                {props.type}
              </span>
              <span className="dashboard-card-items">
                Created at <br />
                {props.created_at}
              </span>
              <span className="dashboard-card-items">
                Go Live <br />
                {props.live}
              </span>
              <span className="dashboard-card-items">
                Status Task <br />
                Open : {props.total_open_task === null? 0 : props.total_open_task} <br />
                Re-open : {props.total_reopen_task === null? 0 : props.total_reopen_task}
              </span>
              <span className="dashboard-card-items">
                Done : {props.total_done_task === null? 0 : props.total_done_task} <br />
                Completed : {props.total_complete_task === null? 0 : props.total_complete_task}
              </span>
            </div>
          </CardBody>
        </Card>
      </Col>
    </React.Fragment>
  )
}

export default withRouter(DashboardCard)
