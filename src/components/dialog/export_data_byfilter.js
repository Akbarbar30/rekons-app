import React, { Component } from "react"
import { withStyles } from "@material-ui/core/styles"
import Dialog from "@material-ui/core/Dialog"
import DialogContent from "@material-ui/core/DialogContent"
import MuiDialogTitle from "@material-ui/core/DialogTitle"
import IconButton from "@material-ui/core/IconButton"
import CloseIcon from "@material-ui/icons/Close"
import { Row, Col, Button } from "reactstrap"
import MUIDataTable from "mui-datatables"
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles"
import { FormGroup, FormLabel, TextField } from '@material-ui/core';
import moment from "moment";
import ReactExport from "react-export-excel";
import { CSVLink, CSVDownload } from "react-csv";


const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: "1px solid #34B966",
        margin: 0,
        padding: 10
    },
    closeButton: {
        position: "absolute",
        right: 5,
        top: 0,
        color: "#34B966"
    }
}))(props => {
    const { children, classes, onClose } = props
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <span className="avenir-black-primary ml-4 mb-0" style={{ fontSize: "18px" }}>
                {children}
            </span>
            {onClose ? (
                <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    )
})

export default class DetailPotongan extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
            MUITableHead: {
                root: {
                    boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)",
                },
            },
            MUIDataTableHeadCell: {
                root: {
                    whiteSpace: 'nowrap',
                },
            },
            MUIDataTableBodyCell: {
                root: {
                    whiteSpace: 'nowrap'
                }
            },

        }
    });


    columnsMP = [
        {
            name: "NO",
            id: "no",
            label: "ID",
            options: {
                filter: false,
                sort: true
            }
        },
        {
            name: "channel",
            id: "channel",
            label: "Channel",
            options: {
                filter: true,
                sort: true
            }
        },
        {
            name: "transaction_id",
            id: "transaction_id",
            label: "Transaction ID",
            options: {
                filter: false,
                sort: true
            }
        },
        {
            name: "Tanggal Pembayaran",
            id: "tgl_bayar",
            label: "Tanggal Pembayaran",
            options: {
                filter: true,
                filterType: 'custom',
                customFilterListRender: v => {
                    if (v['min'] && v['max']) {
                        return `Start Date: ${v['min']}, End Date: ${v['max']}`;
                    } else if (v['min']) {
                        return `Start Date: ${v['min']}`;
                    } else if (v['max']) {
                        return `End Date: ${v['max']}`;
                    }
                    return false;
                },
                filterOptions: {
                    names: [],
                    logic(tanggal, filters) {
                        var tgl = new Date(tanggal)
                        var max = new Date(filters['max'])
                        var min = new Date(filters['min'])
                        if (filters['min'] && filters['max']) {
                            return tgl < min || tgl > max;
                        } else if (filters['min']) {
                            return tgl < min;
                        } else if (filters['max']) {
                            return tgl > max;
                        }
                        return false;
                    },
                    display: (filterList, onChange, index, column) => (
                        <div>
                            <FormLabel>Tanggal Pembayaran</FormLabel>
                            <FormGroup row>
                                <TextField
                                    label="Start Date"
                                    type="date"
                                    value={filterList[index]['min'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['min'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%', marginRight: '5%' }}
                                />
                                <TextField
                                    label="End Date"
                                    type="date"
                                    value={filterList[index]['max'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['max'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%' }}
                                />
                            </FormGroup>
                        </div>
                    ),
                },
            },
        },
        {
            name: "total_bayar",
            id: "total_bayar",
            label: "Total Pembayaran",
            options: {
                filter: false,
                sort: true
            },
        },
        {
            name: "customer",
            id: "customer",
            label: "Customer Contact",
            options: {
                filter: false,
                sort: true
            },
        },
        {
            name: "status",
            id: "status",
            label: "Status",
            options: {
                filter: false,
                sort: true
            },
        }

    ];


    optionsMP = {
        filter: true,
        selectableRows: 'none',
        print: false,
        filterType: 'multiselect',
        pagination: true,
        elevation: 0,
        rowsPerPage: 5,
        responsive: 'scroll',
        onTableInit: this.handleTableChange,
        onTableChange: this.handleTableChange,
        download: false
    };

    render() {
        const data = this.props.data.map((file, index) => [
            parseInt(`${index + 1}`),
            `${file[1]}`,
            `${file[2]}`,
            `${moment(file[3]).format("YYYY-MM-DD HH:mm:ss")}`,
            parseInt(`${file[4]}`),
            `${file[7]}`,
            `${file[8]}`,
        ])
        const headers = [
            { label: "No", key: "id" },
            { label: "Channel", key: "channel" },
            { label: "Transaction ID", key: "trx_id" },
            { label: "Tanggal Transaksi", key: "tgl_pembayaran" },
            { label: "Total Nominal", key: "total_nominal" },
            { label: "Customer Contact", key: "customer" },
            { label: "Status", key: "status" }
        ];
        const dataCSV = this.props.data === [] ? [] : this.props.data.map((file, index) => {
            return {
                id: parseInt(`${index + 1}`),
                channel: `${file[1]}`,
                trx_id: `${file[2]}`,
                tgl_pembayaran: `${moment(file[3]).format("YYYY-MM-DD HH:mm:ss")}`,
                total_nominal: parseInt(`${file[4]}`),
                customer: `${file[7]}`,
                status: `${file[8]}`,
            }
        });
        console.log(dataCSV)
        const { isDialogOpen, handleDialogClose } = this.props
        return (
            <Dialog open={isDialogOpen} onClose={handleDialogClose} fullWidth={true} maxWidth="md">
                <DialogTitle onClose={handleDialogClose}>
                    List Transaksi
                </DialogTitle>
                <DialogContent>
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                        <MUIDataTable title="Data Export" data={data} columns={this.columnsMP} options={this.optionsMP} />
                    </MuiThemeProvider>
                    <Row style={{ margin: "1.5rem 3rem" }} className="add-new-users">
                        <Col md="6" style={{ padding: "0.25rem " }}>
                            <ExcelFile
                                element={
                                    <Button color="primary" className="btn-add-new" size="sm">
                                        Export Excel
                                </Button>}
                                filename={`${moment().format('L')}-transaksiFiltered`}
                            >
                                <ExcelSheet data={dataCSV} name="transaksi">
                                    <ExcelColumn label="No" value="id" />
                                    <ExcelColumn label="Channel" value="channel" />
                                    <ExcelColumn label="Transaction ID" value="trx_id" />
                                    <ExcelColumn label="Tanggal Transaksi" value="tgl_pembayaran" />
                                    <ExcelColumn label="Total Nominal" value="total_nominal" />
                                    <ExcelColumn label="Customer Contact" value="customer" />
                                    <ExcelColumn label="Status" value="status" />
                                </ExcelSheet>
                            </ExcelFile>
                        </Col>
                        <Col md="6" style={{ padding: "0.25rem" }} >
                            <CSVLink filename={`${moment().format('L')}-transaksiFiltered.csv`} data={dataCSV} headers={headers} className="btn btn-primary" style={{ padding: "0.5rem 1.6rem", fontSize: 12 }}>
                                Export CSV
                            </CSVLink>
                        </Col>
                    </Row>
                </DialogContent>
            </Dialog>

        )
    }
}
