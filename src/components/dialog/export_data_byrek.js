import React, { Component } from "react"
import { withStyles } from "@material-ui/core/styles"
import Dialog from "@material-ui/core/Dialog"
import DialogContent from "@material-ui/core/DialogContent"
import MuiDialogTitle from "@material-ui/core/DialogTitle"
import IconButton from "@material-ui/core/IconButton"
import CloseIcon from "@material-ui/icons/Close"
import { Row, Col, Button } from "reactstrap"
import MUIDataTable from "mui-datatables"
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles"
import { FormGroup, FormLabel, TextField } from '@material-ui/core';
import moment from "moment";
import ReactExport from "react-export-excel";
import { CSVLink, CSVDownload } from "react-csv";


const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: "1px solid #34B966",
        margin: 0,
        padding: 10
    },
    closeButton: {
        position: "absolute",
        right: 5,
        top: 0,
        color: "#34B966"
    }
}))(props => {
    const { children, classes, onClose } = props
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <span className="avenir-black-primary ml-4 mb-0" style={{ fontSize: "18px" }}>
                {children}
            </span>
            {onClose ? (
                <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    )
})

export default class ExportDataRek extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
            MUITableHead: {
                root: {
                    boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)",
                },
            },
            MUIDataTableHeadCell: {
                root: {
                    whiteSpace: 'nowrap',
                },
            },
            MUIDataTableBodyCell: {
                root: {
                    whiteSpace: 'nowrap'
                }
            },

        }
    });


    columns = [
        {
            name: "id",
            id: "id",
            label: "No",
            options: {
                sort: true,

            }
        },
        {
            name: "username_penerima",
            id: "username_penerima",
            label: "Username Penerima",
            options: {
                sort: false,
                filter: false
            }
        },
        {
            name: "nama_penerima",
            id: "nama_penerima",
            label: "Nama Penerima",
            options: {
                sort: false,
                filter: false
            }
        },
        {
            name: "bank_penerima",
            id: "bank_penerima",
            label: "Bank Penerima",
            options: {
                sort: false,
                filter: false
            }
        },
        {
            name: "no_rekening_penerima",
            id: "no_rekening_penerima",
            label: "No Rekening Penerima",
            options: {
                sort: false,
                filter: true,
                filterOptions: {
                    names: ['Tidak Lengkap', 'Lengkap'],
                    logic(rek_penerima, filterVal) {
                        const show =
                            (filterVal.indexOf('Tidak Lengkap') >= 0 && (rek_penerima === "" || rek_penerima === " ")) ||
                            (filterVal.indexOf('Lengkap') >= 0 && (rek_penerima !== "" && rek_penerima !== " "));
                        return !show;
                    },
                },
            }
        },
        {
            name: "nama_rekening_penerima",
            id: "nama_rekening_penerima",
            label: "Nama Rekening Penerima",
            options: {
                sort: false,
                filter: false
            }
        },
        {
            name: "jumlah_transaksi",
            id: "jumlah_transaksi",
            label: "Jumlah Transaksi",
            options: {
                sort: false,
                filter: false
            }
        },
        {
            name: "nominal_transaksi",
            id: "nominal_transaksi",
            label: "Nominal Transaksi",
            options: {
                sort: true,
                filter: true,
                filterOptions: {
                    names: ['< Rp.500.000', '>= Rp.500.000'],
                    logic(nominal, filterVal) {

                        const show =
                            (filterVal.indexOf('< Rp.500.000') >= 0 && nominal < 500000) ||
                            (filterVal.indexOf('>= Rp.500.000') >= 0 && nominal >= 500000);
                        return !show;
                    },
                },

            }
        }
    ];


    optionsMP = {
        filter: false,
        selectableRows: 'none',
        print: false,
        filterType: 'multiselect',
        pagination: true,
        elevation: 0,
        rowsPerPage: 5,
        responsive: 'scroll',
        onTableInit: this.handleTableChange,
        onTableChange: this.handleTableChange,
        download: false
    };

    render() {
        const data = this.props.data.map((file, index) => [
            parseInt(`${index + 1}`),
            `${file[1]}`,
            `${file[2]}`,
            `${file[3]}`,
            `${file[4]}`,
            `${file[5]}`,
            parseInt(`${file[6]}`),
            parseInt(`${file[9]}`),
        ])
        console.log(data)
        const dataCSV = this.props.data === [] ? [] : this.props.data.map((file, index) => {
            return {
                id: parseInt(`${index + 1}`),
                username_penerima: `${file[1]}`,
                nama_penerima: `${file[2]}`,
                bank_penerima: `${file[3]}`,
                no_rekening_penerima: `${file[4]}`,
                nama_rekening_penerima: `${file[5]}`,
                jumlah_transaksi: parseInt(`${file[6]}`),
                total_nominal: parseInt(`${file[9]}`)
            }
        });
        console.log(dataCSV)
        const { isDialogOpen, handleDialogClose } = this.props
        return (
            <Dialog open={isDialogOpen} onClose={handleDialogClose} fullWidth={true} maxWidth="md">
                <DialogTitle onClose={handleDialogClose}>
                    List Transaksi
                </DialogTitle>
                <DialogContent>
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                        <MUIDataTable title="Data Export" data={data} columns={this.columns} options={this.optionsMP} />
                    </MuiThemeProvider>
                    <Row style={{ margin: "1.5rem 3rem" }} className="add-new-users">
                        <Col md="6" style={{ padding: "0.25rem " }}>
                            <ExcelFile
                                element={
                                    <Button color="primary" className="btn-add-new" size="sm">
                                        Export Excel
                                </Button>}
                                filename={`${moment().format('L')}-Masjid`}
                            >
                                <ExcelSheet data={dataCSV} name="transaksi">
                                    <ExcelColumn label="No" value="id" />
                                    <ExcelColumn label="Username Penerima" value="username_penerima" />
                                    <ExcelColumn label="Nama Penerima" value="nama_penerima" />
                                    <ExcelColumn label="Bank Penerima" value="bank_penerima" />
                                    <ExcelColumn label="No Rekening Penerima" value="no_rekening_penerima" />
                                    <ExcelColumn label="Nama Rekening Penerima" value="nama_rekening_penerima" />
                                    <ExcelColumn label="Jumlah Transaksi" value="jumlah_transaksi" />
                                    <ExcelColumn label="Total Nominal" value="total_nominal" />
                                </ExcelSheet>
                            </ExcelFile>
                        </Col>
                        <Col md="6" style={{ padding: "0.25rem" }} >
                        </Col>
                    </Row>
                </DialogContent>
            </Dialog>

        )
    }
}
