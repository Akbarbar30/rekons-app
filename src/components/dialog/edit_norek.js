import React, { Component } from "react"
import { Row, Col, Button } from "reactstrap"
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { withStyles } from "@material-ui/core/styles"
import Dialog from "@material-ui/core/Dialog"
import DialogContent from "@material-ui/core/DialogContent"
import MuiDialogTitle from "@material-ui/core/DialogTitle"
import IconButton from "@material-ui/core/IconButton"
import CloseIcon from "@material-ui/icons/Close"
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

const variantIcon = {
    success: CheckCircleIcon,
};

const styles1 = theme => ({
    success: {
        backgroundColor: '#34B966',
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.dark,
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing.unit,
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
});

function MySnackbarContent(props) {
    const { classes, className, message, onClose, variant, ...other } = props;
    const Icon = variantIcon[variant];

    return (
        <SnackbarContent
            className={classNames(classes[variant], className)}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                    <Icon className={classNames(classes.icon, classes.iconVariant)} />
                    {message}
                </span>
            }
            action={[
                <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    className={classes.close}
                    onClick={onClose}
                >
                    <CloseIcon className={classes.icon} />
                </IconButton>,
            ]}
            {...other}
        />
    );
}

MySnackbarContent.propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    message: PropTypes.node,
    onClose: PropTypes.func,
    variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: "1px solid #34B966",
        margin: 0,
        padding: 10
    },
    closeButton: {
        position: "absolute",
        right: 5,
        top: 5,
        color: "#34B966"
    }
}))(props => {
    const { children, classes, onClose } = props
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <span className="avenir-black-primary ml-4 mb-0" style={{ fontSize: "24px" }}>
                {children}
            </span>
            {onClose ? (
                <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    )
})

export default class EditDataRek extends Component {
    constructor(props) {
        super(props);
        this.state = {
            response: null,
            open: false
        };
    }

    onSubmit = async (event, values) => {
        event.preventDefault();
        console.log("value formnya : ",values)

        var form = {
            nama_penerima: values.nama_penerima,
            no_rek: values.no_rek,
            nama_pemilik_rek: values.nama_pemilik_rek,
            nama_bank: values.nama_bank,
            id_data: this.props.detailDataRek.id,
            channel: this.props.detailDataRek.channel
        }

        const url = "http://rekons.immobisp.com:3005/rekons/"

        await fetch(url, {
            method: "PUT",
            body: JSON.stringify(form),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(res => {
                return res.json()
            })
            .then(response => {
                if (response.status === 'success') {
                    this.setState({
                        open: true,
                    })
                    setTimeout(() => { window.location.reload() }, 3000);
                }
            })
            .catch(error => console.error('Error:', error));
    }

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ open: false });
    };

    render() {
        const { isDialogOpen, handleDialogClose, detailDataRek } = this.props
        const defaultValues = {
            no_rek: detailDataRek === null || detailDataRek === "null"? ' ' : detailDataRek.no_rekening_penerima,
            nama_penerima: detailDataRek === null || detailDataRek === "null" ? ' ' : detailDataRek.nama_penerima,
            nama_pemilik_rek: detailDataRek === null || detailDataRek === "null" ? ' ' : detailDataRek.nama_rekening_penerima,
            nama_bank: detailDataRek === null ? ' ' : detailDataRek.bank_penerima
        };
        
        return (
            <>
                {detailDataRek !== null || detailDataRek !== {} ? (
                    <Dialog open={isDialogOpen} onClose={handleDialogClose} fullWidth={true} maxWidth="md">
                        <Snackbar
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={this.state.open}
                            autoHideDuration={6000}
                            onClose={this.handleClose}
                        >
                            <MySnackbarContentWrapper
                                onClose={this.handleClose}
                                variant="success"
                                message="Data Rekening Terupdate"
                            />
                        </Snackbar>
                        <DialogTitle onClose={handleDialogClose}>Edit Data Rekening</DialogTitle>
                        <DialogContent>
                            <AvForm onValidSubmit={this.onSubmit} model={defaultValues}>
                                <Row style={{ margin: "1.5rem 3rem" }} className="add-new-users">
                                    <Col md="6" style={{ padding: "0.25rem " }}>
                                        <AvGroup>
                                            <AvField type="text" name="nama_pemilik_rek" label="Nama Rekening Penerima" validate={{
                                                required: { value: true, errorMessage: 'Nama Rekening Penerima required' }
                                            }} />
                                        </AvGroup>
                                        <AvGroup>
                                            <AvField type="text" name="no_rek" label="Nomor Rekening Penerima" validate={{
                                                required: { value: true, errorMessage: 'Nomor Rekening Penerima required' },
                                                pattern: { value: '^[0-9]+$', errorMessage: 'Nama Rekening Penerima must be composed only with number' },
                                                minLength: { value: 10, errorMessage: 'Nomor Rekening Penerima must be between 10 and 15 characters' },
                                                maxLength: { value: 15, errorMessage: 'Nomor Rekening Penerima must be between 10 and 15 characters' }
                                            }} />
                                        </AvGroup>
                                    </Col>
                                    <Col md="6" style={{ padding: "0.25rem" }}>
                                        <AvGroup>
                                            <AvField type="text" name="nama_penerima" label="Nama Penerima" validate={{
                                                required: { value: true, errorMessage: 'Nama Penerima required' }
                                            }} />
                                        </AvGroup>
                                        <AvGroup>
                                            <AvField type="text" name="nama_bank" label="Nama Rekening Bank" validate={{
                                                required: { value: true, errorMessage: 'Nama Rekening Bank required' }
                                            }} />
                                        </AvGroup>
                                    </Col>
                                    <Button
                                            type="submit"
                                            color="primary"
                                            className="px-4"
                                            style={{ fontSize: 16, position: "flex", bottom: 0, right: 0, marginTop: "10%" }}
                                        >
                                            Save
                                    </Button>
                                </Row>
                            </AvForm>
                        </DialogContent>
                    </Dialog>

                ) : (
                        <div></div>
                    )}
            </>
        )
    }
}
