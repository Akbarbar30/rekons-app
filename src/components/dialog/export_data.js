import React, { Component } from "react"
import { withStyles } from "@material-ui/core/styles"
import Dialog from "@material-ui/core/Dialog"
import DialogContent from "@material-ui/core/DialogContent"
import MuiDialogTitle from "@material-ui/core/DialogTitle"
import IconButton from "@material-ui/core/IconButton"
import CloseIcon from "@material-ui/icons/Close"
import { Row, Col} from "reactstrap"
import MUIDataTable from "mui-datatables"
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles"
import { FormGroup, FormLabel, TextField } from '@material-ui/core';
import moment from "moment";
// import ReactExport from "react-export-excel";
import { CSVLink } from "react-csv";
import ReactHTMLTableToExcel from "react-html-table-to-excel"


// const ExcelFile = ReactExport.ExcelFile;
// const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
// const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: "1px solid #34B966",
        margin: 0,
        padding: 10
    },
    closeButton: {
        position: "absolute",
        right: 5,
        top: 0,
        color: "#34B966"
    }
}))(props => {
    const { children, classes, onClose } = props
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <span className="avenir-black-primary ml-4 mb-0" style={{ fontSize: "18px" }}>
                {children}
            </span>
            {onClose ? (
                <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    )
})

export default class DetailPotongan extends Component {
    constructor(props) {
        super(props)
        this.state = {
            array: [],
            subtotal: 0,
            grandtotal: 0

        }
    }

    componentWillUnmount() {
        this.setState({
            array: []
        })
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
            MUITableHead: {
                root: {
                    boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)",
                },
            },
            MUIDataTableHeadCell: {
                root: {
                    whiteSpace: 'nowrap',
                },
            },
            MUIDataTableBodyCell: {
                root: {
                    whiteSpace: 'nowrap'
                }
            },

        }
    });


    columnsMP = [
        {
            name: "NO",
            id: "no",
            label: "ID",
            options: {
                filter: false,
                sort: true
            }
        },
        {
            name: "channel",
            id: "channel",
            label: "Channel",
            options: {
                filter: false,
                sort: false
            }
        },
        {
            name: "bill_reff",
            id: "bill_reff",
            label: "Bill Reff",
            options: {
                filter: false,
                sort: false
            }
        },
        {
            name: "tgl_bayar",
            id: "tgl_bayar",
            label: "Tanggal Bayar",
            options: {
                filter: true,
                sort: true,
                filterType: 'custom',
                customFilterListRender: v => {
                    if (v['min'] && v['max']) {
                        return `Start Date: ${v['min']}, End Date: ${v['max']}`;
                    } else if (v['min']) {
                        return `Start Date: ${v['min']}`;
                    } else if (v['max']) {
                        return `End Date: ${v['max']}`;
                    }
                    return false;
                },
                filterOptions: {
                    names: [],
                    logic(tanggal, filters) {
                        var check = new Date(tanggal);
                        var from = new Date(filters['min']);
                        var to = new Date(filters['max']);
                        from.setDate(from.getDate());
                        to.setDate(to.getDate());
                        from = new Date(from).setHours(0, 0, 0, 0);
                        to = new Date(to).setHours(23, 59, 59, 59);

                        if (filters['min'] && filters['max'] && check >= to && check <= from) {
                            return true;
                        } else if (filters['min'] && check >= to) {
                            console.log(new Date(check).toString());
                            console.log(new Date(to).toString());
                            return true;
                        } else if (filters['max'] && check <= from) {
                            console.log(new Date(check).toString());
                            console.log(new Date(from).toString());
                            return true;
                        }
                        return false;
                    },
                    display: (filterList, onChange, index, column) => (
                        <div>
                            <FormLabel>Tanggal Pembayaran</FormLabel>
                            <FormGroup row>
                                <TextField
                                    label="Start Date"
                                    type="date"
                                    value={filterList[index]['min'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['min'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%', marginRight: '5%' }}
                                />
                                <TextField
                                    label="End Date"
                                    type="date"
                                    value={filterList[index]['max'] || ''}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={event => {
                                        filterList[index]['max'] = event.target.value;
                                        onChange(filterList[index], index, column);
                                    }}
                                    style={{ width: '45%' }}
                                />
                            </FormGroup>
                        </div>
                    ),
                },
            },
        },
        {
            name: "sender",
            id: "sender",
            label: "Username Pengirim",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "receiver",
            id: "receiver",
            label: "Username Penerima",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "total_bayar_awal",
            id: "total_bayar_awal",
            label: "Total Nominal Awal",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "total_bayar_akhir",
            id: "total_bayar_akhir",
            label: "Total Nominal Akhir",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "nama_penerima",
            id: "nama_penerima",
            label: "Nama Pennerima",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "nama_rek",
            id: "nama_rek",
            label: "Nama Rekening",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "bank",
            id: "bank",
            label: "Bank Penerima",
            options: {
                filter: false,
                sort: false
            },
        },
        {
            name: "rek_penerima",
            id: "rek_penerima",
            label: "No Rekening Penerima",
            options: {
                filter: true,
                sort: false,
                filterOptions: {
                    names: ['Tidak Lengkap', 'Lengkap'],
                    logic(rek_penerima, filterVal) {
                        const show =
                            (filterVal.indexOf('Tidak Lengkap') >= 0 && (rek_penerima === "" || rek_penerima === " ")) ||
                            (filterVal.indexOf('Lengkap') >= 0 && (rek_penerima !== "" && rek_penerima !== " "));
                        return !show;
                    },
                },
            },
        },
        {
            name: "isTransfer",
            id: "isTransfer",
            label: "Transfer Bank",
            options: {
                filter: true,
                sort: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    if (value === "T") {
                        return (
                            <i className={`fas fa-check`} style={{ color: 'green' }} />
                        )
                    } else if (value === "F") {
                        return <div></div>
                    }
                },
                filterOptions: {
                    names: ['Belum Transfer', 'Sudah Transfer'],
                    logic(salary, filterVal) {

                        const show =
                            (filterVal.indexOf('Belum Transfer') >= 0 && salary === "F") ||
                            (filterVal.indexOf('Sudah Transfer') >= 0 && salary === "T");
                        return !show;
                    },
                },
            },
        },

    ];


    optionsMP = {
        filter: true,
        selectableRows: 'none',
        print: false,
        filterType: 'multiselect',
        pagination: true,
        elevation: 0,
        rowsPerPage: 5,
        responsive: 'scroll',
        onTableInit: this.handleTableChange,
        onTableChange: this.handleTableChange,
        download: false
    };

    getByValue2(arr, value) {
        console.log("value pemfilter : ", value)
        var result = arr.filter(function (o) { return o.receiver === value; });

        return result ? result[0] : null; // or undefined

    }

    generateRow(index, row) {
        const { array } = this.state
        const { rekapmasjid, data } = this.props
        var newObj = {};
        var subtotalawal = 0;
        var subtotalakhir = 0
        var subtotalpotongankmdn = 0;
        var subtotalpotonganchannel = 0
        var subtotalpoinrejeki = 0;
        if (row === 0) {
            array.push(index.receiver)
            return (
                <tr>
                    <td>'{index.transaction_id.toString()}</td>
                    <td>{moment(index.tgl_pembayaran).format("YYYY-MM-DD HH:mm:ss")}</td>
                    <td>{index.channel}</td>
                    <td>{index.sender.toString()}</td>
                    <td>{index.receiver.toString()}</td>
                    <td>{index.nama_penerima}</td>
                    <td>{index.bank_penerima}</td>
                    <td>{index.nama_rekening_penerima}</td>
                    <td>{index.no_rekening_penerima.toString()}</td>
                    <td>{index.total_pembayaran}</td>
                    <td>{index.nominal_potongan_kmdn}</td>
                    <td>{index.nominal_potongan_channel}</td>
                    <td>{index.nominal_potongan_cashback}</td>
                    <td>{index.total_akhir}</td>
                    <td>{index.nominal_potongan_cashback}</td>
                </tr>
            )
        } else {
            if (index.receiver !== array[array.length - 1]) {
                console.log("array : ", array)
                var valuefilter = array[array.length - 1] === undefined ? array[array.length] : array[array.length - 1]
                newObj = this.getByValue2(rekapmasjid, valuefilter)
                console.log("masuk if tidak sama : ", newObj)
                if (newObj === undefined) {
                    subtotalawal = 0;
                    subtotalakhir = 0;
                    subtotalpotongankmdn = 0;
                    subtotalpotonganchannel = 0;
                    subtotalpoinrejeki = 0;
                } else {
                    subtotalawal = newObj.nominal_transaksi_awal;
                    subtotalakhir = newObj.total_akhir;
                    subtotalpotongankmdn = newObj.nominal_potongan_kmdn;
                    subtotalpotonganchannel = newObj.nominal_potongan_channel;
                    subtotalpoinrejeki = newObj.nominal_potongan_cashback;
                }
                array.push(index.receiver)
                return (
                    <div>
                        <tr style={{backgroundColor:"#FFE4B5"}}>
                            <td colSpan="9">Sub Total</td>
                            <td>{subtotalawal}</td>
                            <td>{subtotalpotongankmdn}</td>
                            <td>{subtotalpotonganchannel}</td>
                            <td>{subtotalpoinrejeki}</td>
                            <td>{subtotalakhir}</td>
                            <td>{subtotalpoinrejeki}</td>
                        </tr>
                        <tr>
                            <td>'{index.transaction_id.toString()}</td>
                            <td>{moment(index.tgl_pembayaran).format("YYYY-MM-DD HH:mm:ss")}</td>
                            <td>{index.channel}</td>
                            <td>{index.sender.toString()}</td>
                            <td>{index.receiver.toString()}</td>
                            <td>{index.nama_penerima}</td>
                            <td>{index.bank_penerima}</td>
                            <td>{index.nama_rekening_penerima}</td>
                            <td>{index.no_rekening_penerima.toString()}</td>
                            <td>{index.total_pembayaran}</td>
                            <td>{index.nominal_potongan_kmdn}</td>
                            <td>{index.nominal_potongan_channel}</td>
                            <td>{index.nominal_potongan_cashback}</td>
                            <td>{index.total_akhir}</td>
                            <td>{index.nominal_potongan_cashback}</td>
                        </tr>
                    </div>
                )
            } else if (index.receiver === array[array.length - 1]) {
                if (row === data.length - 1) {
                    newObj = this.getByValue2(rekapmasjid, index.receiver)
                    console.log("masuk rowterakhir : ", newObj)
                    if (newObj === undefined) {
                        subtotalawal = 0;
                        subtotalakhir = 0;
                        subtotalpotongankmdn = 0;
                        subtotalpotonganchannel = 0;
                        subtotalpoinrejeki = 0;
                    } else {
                        subtotalawal = newObj.nominal_transaksi_awal;
                        subtotalakhir = newObj.total_akhir;
                        subtotalpotongankmdn = newObj.nominal_potongan_kmdn;
                        subtotalpotonganchannel = newObj.nominal_potongan_channel;
                        subtotalpoinrejeki = newObj.nominal_potongan_cashback;
                    }
                    array.push(index.receiver)
                    return (
                        <div>
                            <tr>
                                <td>'{index.transaction_id.toString()}</td>
                                <td>{moment(index.tgl_pembayaran).format("YYYY-MM-DD HH:mm:ss")}</td>
                                <td>{index.channel}</td>
                                <td>{index.sender.toString()}</td>
                                <td>{index.receiver.toString()}</td>
                                <td>{index.nama_penerima}</td>
                                <td>{index.bank_penerima}</td>
                                <td>{index.nama_rekening_penerima}</td>
                                <td>{index.no_rekening_penerima.toString()}</td>
                                <td>{index.total_pembayaran}</td>
                                <td>{index.nominal_potongan_kmdn}</td>
                                <td>{index.nominal_potongan_channel}</td>
                                <td>{index.nominal_potongan_cashback}</td>
                                <td>{index.total_akhir}</td>
                                <td>{index.nominal_potongan_cashback}</td>
                            </tr>
                            <tr style={{backgroundColor:"#FFE4B5"}}>
                                <td colSpan="9">Sub Total</td>
                                <td>{subtotalawal}</td>
                                <td>{subtotalpotongankmdn}</td>
                                <td>{subtotalpotonganchannel}</td>
                                <td>{subtotalpoinrejeki}</td>
                                <td>{subtotalakhir}</td>
                                <td>{subtotalpoinrejeki}</td>
                            </tr>
                        </div>
                    )
                } else {
                    array.push(index.receiver)
                    return (
                        <tr>
                            <td>'{index.transaction_id.toString()}</td>
                            <td>{moment(index.tgl_pembayaran).format("YYYY-MM-DD HH:mm:ss")}</td>
                            <td>{index.channel}</td>
                            <td>{index.sender.toString()}</td>
                            <td>{index.receiver.toString()}</td>
                            <td>{index.nama_penerima}</td>
                            <td>{index.bank_penerima}</td>
                            <td>{index.nama_rekening_penerima}</td>
                            <td>{index.no_rekening_penerima.toString()}</td>
                            <td>{index.total_pembayaran}</td>
                            <td>{index.nominal_potongan_kmdn}</td>
                            <td>{index.nominal_potongan_channel}</td>
                            <td>{index.nominal_potongan_cashback}</td>
                            <td>{index.total_akhir}</td>
                            <td>{index.nominal_potongan_cashback}</td>
                        </tr>
                    )
                }
            }
        }
    }

    // generateRow(index, row) {
    //          const { rekapmasjid, data } = this.props
    //          console.log(rekapmasjid, data)
    //         return (
    //             <tr>
    //                 <td>'{index.transaction_id.toString()}</td>
    //                 <td>{moment(index.tgl_pembayaran).format("YYYY-MM-DD HH:mm:ss")}</td>
    //                 <td>{index.channel}</td>
    //                 <td>{index.sender.toString()}</td>
    //                 <td>{index.receiver.toString()}</td>
    //                 <td>{index.nama_penerima}</td>
    //                 <td>{index.bank_penerima}</td>
    //                 <td>{index.nama_rekening_penerima}</td>
    //                 <td>{index.no_rekening_penerima.toString()}</td>
    //                 <td>{index.total_pembayaran}</td>
    //                 <td>{index.nominal_potongan_kmdn}</td>
    //                 <td>{index.nominal_potongan_channel}</td>
    //                 <td>{index.nominal_potongan_cashback}</td>
    //                 <td>{index.total_akhir}</td>
    //                 <td>{index.nominal_potongan_cashback}</td>
    //             </tr>
    //         )
    // }

    render() {
        const dataMP = this.props.data === [] ? [] : this.props.data.map((file, index) => [
            parseInt(`${index + 1}`),
            `${file.channel}`,
            `${file.bill_no}`,
            `${moment(file.tgl_pembayaran).format("YYYY-MM-DD HH:mm:ss")}`,
            `${file.sender}`,
            `${file.receiver}`,
            `${file.total_pembayaran}`,
            `${file.total_akhir}`,
            `${file.nama_penerima === "null" ? " " : file.nama_penerima}`,
            `${file.nama_rekening_penerima === "null" ? " " : file.nama_rekening_penerima}`,
            `${file.bank_penerima === "null" ? " " : file.bank_penerima}`,
            `${file.no_rekening_penerima === "null" ? " " : file.no_rekening_penerima}`,
            `${file.isTransfer}`

        ])
        const headers = [
            { label: "Tanggal Transaksi", key: "tgl_pembayaran" },
            { label: "Channel", key: "channel" },
            { label: "Sender Username", key: "sender" },
            { label: "Receiver Username", key: "receiver" },
            { label: "Nama Penerima", key: "nama_penerima" },
            { label: "Nama Bank", key: "bank_penerima" },
            { label: "Nama Rekening Penerima", key: "nama_rekening_penerima" },
            { label: "No Rekening", key: "no_rekening_penerima" },
            { label: "Total Nominal", key: "total_akhir" }
        ];
        const dataCSV = this.props.data === [] ? [] : this.props.data;

        const { isDialogOpen, handleDialogClose } = this.props
        return (
            <>
                {dataMP === [] ? (
                    <>
                        <div></div>
                    </>
                ) : (
                        <Dialog open={isDialogOpen} onClose={handleDialogClose} fullWidth={true} maxWidth="md">
                            <DialogTitle onClose={handleDialogClose}>
                                List Transaksi
                        </DialogTitle>
                            <DialogContent>
                                <MuiThemeProvider theme={this.getMuiTheme()}>
                                    <MUIDataTable title="Data Export" data={dataMP} columns={this.columnsMP} options={this.optionsMP} />
                                </MuiThemeProvider>
                                <Row style={{ margin: "1.5rem 3rem" }} className="add-new-users">
                                    <Col md="6" style={{ padding: "0.25rem " }}>
                                        <div>
                                            <ReactHTMLTableToExcel
                                                id="test-table-xls-button"
                                                className="btn btn-primary btn-add-new"
                                                style={{ padding: "0.5rem 1.6rem", fontSize: 12 }}
                                                table="table-to-xls"
                                                filename={`${moment().format('L')}-transaksi${this.props.namaRek}`}
                                                sheet="tablexls"
                                                buttonText="Export Excel"
                                            />
                                            <table style={{ display: "none" }} id="table-to-xls">
                                                <tr style={{backgroundColor:"#87CEFA"}}>
                                                    <th>Transaction ID</th>
                                                    <th>Tanggal Transaksi</th>
                                                    <th>Channel</th>
                                                    <th>Sender Username</th>
                                                    <th>Receiver Username</th>
                                                    <th>Nama Penerima</th>
                                                    <th>Nama Bank</th>
                                                    <th>Nama Rekening Penerima</th>
                                                    <th>No Rekening</th>
                                                    <th>Total Nominal Awal</th>
                                                    <th>Total Potongan KMDN</th>
                                                    <th>Total Potongan Channel</th>
                                                    <th>Total Rejeki Poin</th>
                                                    <th>Total Nominal Akhir</th>
                                                    <th>Poin</th>
                                                </tr>
                                                {this.props.data.map((index, row) =>
                                                    this.generateRow(index, row)
                                                )}
                                                <tr style={{backgroundColor:"#90EE90"}}>
                                                    <td colSpan="9">Grand Total</td>
                                                    <td>{this.props.nominal_transaksi_awal}</td>
                                                    <td>{this.props.nominal_potongan_kmdn}</td>
                                                    <td>{this.props.nominal_potongan_channel}</td>
                                                    <td>{this.props.nominal_potongan_cashback}</td>
                                                    <td>{this.props.total_akhir}</td>
                                                    <td>{this.props.nominal_potongan_cashback}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </Col>
                                    <Col md="6" style={{ padding: "0.25rem" }} >
                                        <CSVLink filename={`${moment().format('L')}-transaksi${this.props.namaRek}.csv`} data={dataCSV} headers={headers} className="btn btn-primary" style={{ padding: "0.5rem 1.6rem", fontSize: 12 }}>
                                            Export CSV
                                        </CSVLink>
                                    </Col>
                                </Row>
                            </DialogContent>
                        </Dialog>
                    )}
            </>


        )
    }
}
