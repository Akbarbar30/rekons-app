import React, { Component } from "react"
import { withStyles } from "@material-ui/core/styles"
import Dialog from "@material-ui/core/Dialog"
import DialogContent from "@material-ui/core/DialogContent"
import MuiDialogTitle from "@material-ui/core/DialogTitle"
import IconButton from "@material-ui/core/IconButton"
import CloseIcon from "@material-ui/icons/Close"
import { Row, Col } from "reactstrap"

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: "1px solid #34B966",
        margin: 0,
        padding: 10
    },
    closeButton: {
        position: "absolute",
        right: 5,
        top: 0,
        color: "#34B966"
    }
}))(props => {
    const { children, classes, onClose } = props
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <span className="avenir-black-primary ml-4 mb-0" style={{ fontSize: "18px" }}>
                {children}
            </span>
            {onClose ? (
                <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    )
})

export default class DetailPotongan extends Component {

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({ open: false });
    };

    render() {
        const { isDialogOpen, handleDialogClose, detailDataRek } = this.props
        return (
            <>
                {detailDataRek !== null || detailDataRek !== {} ? (
                    <Dialog open={isDialogOpen} onClose={handleDialogClose} fullWidth={true} maxWidth="xs">
                        <DialogTitle onClose={handleDialogClose}>
                            Detail Potongan Transaksi
                        </DialogTitle>
                        <DialogContent>
                            <Row style={{ margin: "1.5rem 3rem" }} className="add-new-users">
                                <Col md="6" style={{ padding: "0.25rem " }}>
                                    Nominal Awal
                                </Col>
                                <Col md="6" style={{ padding: "0.25rem" }} align="right">
                                    Rp. {detailDataRek === null || detailDataRek === "null" ? ' ' : detailDataRek.total_pembayaran}
                                </Col>
                            </Row>
                            <Row style={{ margin: "1.5rem 3rem" }} className="add-new-users">
                                <Col md="6" style={{ padding: "0.25rem ",  whiteSpace: "nowrap" }}>
                                    Potongan {detailDataRek === null || detailDataRek === "null" ? ' ' : detailDataRek.channel }
                                    ( {detailDataRek === null || detailDataRek === "null" ? ' ' : detailDataRek.potongan_channel * 100} %)
                                </Col>
                                <Col md="6" style={{ padding: "0.25rem" }} align="right">
                                    - Rp. {detailDataRek === null || detailDataRek === "null" ? ' ' : detailDataRek.nominal_potongan_channel}
                                </Col>
                            </Row>
                            <Row style={{ margin: "1.5rem 3rem" }} className="add-new-users">
                                <Col md="6" style={{ padding: "0.25rem " ,  whiteSpace: "nowrap"}}>
                                    Potongan KMDN
                                    ( {detailDataRek === null || detailDataRek === "null" ? ' ' : detailDataRek.potongan_kmdn * 100} %)
                                </Col>
                                <Col md="6" style={{ padding: "0.25rem"  }} align="right">
                                   - Rp. {detailDataRek === null || detailDataRek === "null" ? ' ' : detailDataRek.nominal_potongan_kmdn}
                                </Col>
                            </Row>
                            <Row style={{ margin: "1.5rem 3rem" }} className="add-new-users">
                                <Col md="6" style={{ padding: "0.25rem ", whiteSpace: "nowrap" }} >
                                    Rejeki Poin
                                    ( {detailDataRek === null || detailDataRek === "null" ? ' ' : detailDataRek.potongan_cashback * 100} %)
                                </Col>
                                <Col md="6" style={{ padding: "0.25rem" }} align="right">
                                    - Rp. {detailDataRek === null || detailDataRek === "null" ? ' ' : detailDataRek.nominal_potongan_cashback}
                                </Col>
                            </Row>
                            <Row style={{ margin: "1.5rem 3rem" }} className="add-new-users">
                                <Col md="6" style={{ padding: "0.25rem " }}>
                                    Nominal Akhir
                                </Col>
                                <Col md="6" style={{ padding: "0.25rem" }} align="right">
                                    Rp. {detailDataRek === null || detailDataRek === "null" ? ' ' : detailDataRek.total_akhir}
                                </Col>
                            </Row>
                        </DialogContent>
                    </Dialog>

                ) : (
                        <div></div>
                    )}
            </>
        )
    }
}
