import merge from 'lodash/merge'

// fetchJSON is bundled wrapper around fetch which simplifies working
// with JSON API:
//   * Automatically adds Content-Type: application/json to request headers
//   * Parses response as JSON when Content-Type: application/json header is
//     present in response headers
//   * Converts non-ok responses to errors
import { configureRefreshFetch, fetchJSON } from 'refresh-fetch'

// Provide your favorite token saving -- to cookies, local storage, ...
// const retrieveToken = () => {
//     localStorage.getItem('token')
// }

// const saveToken = token => {
//     localStorage.setItem('token', token)
// }

// const clearToken = () => {
//     localStorage.removeItem('token')

// }

// Add token to the request headers
const fetchJSONWithToken = (url, options = {}) => {
  const token =  localStorage.getItem('token')

  let optionsWithToken = options
  if (token != null) {
    optionsWithToken = merge({}, options, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  }

  return fetchJSON(url, optionsWithToken)
}

// Decide whether this error returned from API means that we want
// to try refreshing the token. error.response contains the fetch Response
// object, error.body contains the parsed JSON response body
const shouldRefreshToken = response =>
//  console.log(error)
 response.body.message === 'jwt expired'
 
// Do the actual token refreshing and update the saved token

const refreshToken = () => {
  console.log('refresh token')
  return fetchJSONWithToken('/api/refresh-token', {
    method: 'POST'
  })
    .then(response => {
      localStorage.setItem('token', response.body.token)
    //   saveToken(response.body.token)
      return response
    })
    .catch(error => {
      // If we failed by any reason in refreshing, just clear the token,
      // it's not that big of a deal
    //   clearToken()
      localStorage.removeItem('token')
      throw error
    })
}

export const fetchApi = configureRefreshFetch({
  shouldRefreshToken,
  refreshToken,
  fetch: fetchJSONWithToken,
})